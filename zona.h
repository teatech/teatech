#ifndef ZONA_H
#define ZONA_H

#include <map>
#include <QDebug>
#include <QJsonObject>
#include <QList>
#include <opencv2/core/core.hpp>
#include "opencv/cv.h"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "fixation.h"

using namespace std;
//using namespace cv;

class Zona
{
public:
    Zona();
    Zona(QJsonObject JsonObject);

    int frameStart, frameEnd;
    int idZona,idVideo,idEtiqueta;
    bool isReal;
    map<int, QList<cv::Point>> zonasMap;
    QString label; //={ojoI,ojoD,ojos,nariz,boca,cara,distraccion};

    bool isFixIn(Fixation fix);


};

#endif // ZONA_H
