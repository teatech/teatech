#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSignalMapper>
#include <QString>
#include <QFileDialog>
#include <QMessageBox>
#include <QDebug>
#include <iostream>
#include <player.h>
#include <map>
#include <list>
#include <QJsonDocument>
#include <QJsonObject>
#include <QFile>
#include <QFileDevice>
#include <QGridLayout>
#include <cstdlib>
#include <ctime>

#include "qcustomplot.h"
#include "etiqueta.h"
#include "jsondownloader.h"
#include <paciente.h>
#include <video.h>
#include "fixation.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    Ui::MainWindow *ui;

    //FUNCIONES

    //void writeJSON(const QJsonObject &json);

    //VARIABLES GLOBALES
    Player* myPlayer;
    QJsonObject pacientesJSON[20];

    /***Contadores de ID***/
    int contPacienteID, contVideoID;
    int mode;
    /***Objetos seleccionados***/
    Paciente* currentPacient;
    Sesion* currentSesion;
    Video* currentVideo;
    Prueba* currentPrueba;
    QString label;

    /***Listas con todos los objetos***/
    map<int, Paciente*> pacientes;
    map<int, Video*> videos;
    QList<Etiqueta*> etiquetas;

private:
    void createPlayerThread(int mode);
    void getParameters(Prueba *auxPrueba, Sesion *auxSesion);
    void getRealROI(QList<Fixation> auxFix);

    void displaySessions(Paciente* patient);
    void displayTests(map<int, Prueba*> tests);

    float dist(Point a, Point b);
    map<int, Point> readData(String filename);
    JSONdownloader* jsonDownloader;

private slots:
    void FullGaze(void);
    void GoSesion(void);
    void GoSessionFinish(Sesion* session);
    void PlayVideoSelected(void);
    void ShowVideos(void);
    void ShowPacientes(void);
    void AddVideo(void);
    void AddPaciente(void);
    void SubmitPaciente(void);    
    void ShowHistorial();
    void ShowPruebas();
    void PlayVideo(void);
    void Calibrar(void);
    void StartSesion(void);
    void updatePlayerUI(QImage img);
    void VideoTraza();
    void clearLayout(QLayout *layout);
    void resetPlayer();
    void ShowSelectedVideo();
    void SelectROILabel();
    int splitID(QString s);

    int runCaptureExectuable(QString pathToFolder, int idVideo, QString nameVideo);

    void readPatients(QList<Paciente*> patients);
    void readPatientInfo(Paciente* patient, QList<Sesion*> sessions);
    void readNewPatient(Paciente* patient);

    void readSessions(QList<Sesion*> sessions);
    void readSessionInfo(/*Sesion* session,*/ QList<Prueba*> tests);

    void readNewZone(Zona* zone);

    void readVideos(QList<Video*> videos);
    void readVideoInfo(Video* video, QList<Zona*> zones);
    void readNewVideo(Video*);

    void readLabels(QList<Etiqueta*> labels);
    void readNewLabel(Etiqueta* label);

    void setEvent(bool isEvent);
    void setLabel(QString lb);
    void StartEndEvent(void);
    void on_SFixTime_valueChanged(int value);
    void on_SFixRad_valueChanged(int value);
};

//int MainWindow::pacienteID = 0;
//int MainWindow::videoID = 0;
//map<int, Video*> MainWindow::videos;

#endif // MAINWINDOW_H
