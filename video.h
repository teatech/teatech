#ifndef VIDEO_H
#define VIDEO_H

#include <QString>

#include "zona.h"

class Video
{
public:
    Video();
    Video(QJsonObject JsonObject);
    ~Video();

    int idVideo, fps;
    QString nombreVideo, originalFileName;
    map<int, Zona*> zonas;
    map<int, Zona*> zonasReales;

    /***Contadores ID***/
    int contIdZona, contIdZonaReal;

};

#endif // video_H
