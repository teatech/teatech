#include "displayvideo.h"
#include "ui_displayvideo.h"

#include <QMessageBox>

Displayvideo::Displayvideo(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Displayvideo)
{
    ui->setupUi(this);
    Displayvideo::showFullScreen();

    myPlayerTraza = new Player();
    myPlayerTraza -> setMode(1);
    QString filename = "happiness1.mp4";

    if (!filename.isEmpty()){
         if (!myPlayerTraza->loadVideo(filename.toLatin1().data())){
             QMessageBox msgBox;
             msgBox.setText("The selected video could not be opened!");
             msgBox.exec();
         }
     }

    connect(myPlayerTraza, SIGNAL(processedImage(QImage)),this, SLOT(updatePlayerUI(QImage)));
    connect(ui->BPlayTraza,SIGNAL(clicked()),this,SLOT(PlayVideo()));

}

void Displayvideo::updatePlayerUI(QImage img)
{
    if(!img.isNull()){
        ui->LScreenTraza->setAlignment(Qt::AlignCenter);
        ui->LScreenTraza->setPixmap(QPixmap::fromImage(img).scaled(ui->LScreenTraza->size(), Qt::KeepAspectRatio, Qt::FastTransformation));
    }
}

void Displayvideo::PlayVideo()
{
    if(myPlayerTraza->isStopped()){
        myPlayerTraza->Play();
        ui->BPlayTraza->setText(tr("Stop"));
    }
    else{
        myPlayerTraza->Stop();
        ui->BPlayTraza->setText(tr("Play"));
    }
}

Displayvideo::~Displayvideo()
{
    delete myPlayerTraza;
    delete ui;
}
