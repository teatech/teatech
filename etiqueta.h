#ifndef ETIQUETA_H
#define ETIQUETA_H

#include <QString>
#include <QJsonObject>

class Etiqueta
{
public:
    Etiqueta();
    Etiqueta(QJsonObject JsonObject);

    int idEtiqueta;
    QString nombreEtiqueta;
};

#endif // ETIQUETA_H
