#include "roilabel.h"
#include "ui_roilabel.h"
#include <QDebug>

ROILabel::ROILabel(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ROILabel)
{
    ui->setupUi(this);
    connect(this,SIGNAL(accepted()),this,SLOT(eventOrZone()));

    /*ui->CBROI->addItem("Ojo Derec");
    ui->CBROI->addItem("Ojo Izquir");
    ui->CBROI->addItem("Ojos");
    ui->CBROI->addItem("Nariz");
    ui->CBROI->addItem("Boca");
    ui->CBROI->addItem("Cabeza");
    ui->CBROI->addItem("Objeto1");
    ui->CBROI->addItem("Objeto2");
    ui->CBROI->addItem("Eventos");
    ui->CBROI->addItem("Otro");*/
}

ROILabel::~ROILabel()
{
    delete ui;
}

void ROILabel::setLabelList(QList<Etiqueta*> labels){

    foreach(Etiqueta* label, labels){
        ui->CBROI->addItem(label->nombreEtiqueta);
    }

}

void ROILabel::eventOrZone(){
    if(ui->RBEvento->isChecked()){
        emit event_or_not(true);
        qDebug() << "ISS EVENTTT";
    }
    else
        emit event_or_not(false);
qDebug()<<"A";
    QString text = ui->CBROI->currentText();
    qDebug() << "Text is " << text;
    qDebug() << "Going to emit " << (text);
    emit(setLabelName(text));
}
