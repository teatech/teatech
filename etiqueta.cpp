#include "etiqueta.h"

Etiqueta::Etiqueta()
{
    idEtiqueta =0;
}

Etiqueta::Etiqueta(QJsonObject JsonObject){

    if(JsonObject["id"]!=NULL && JsonObject["id"].toString()!="null")
        idEtiqueta = JsonObject["id"].toString().toInt();
    else
        idEtiqueta = -1;

    if(JsonObject["description"]!=NULL)
        nombreEtiqueta = JsonObject["description"].toString();
}
