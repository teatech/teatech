#ifndef PACIENTE_H
#define PACIENTE_H

#include <QString>
#include <QDateTime>
#include <QJsonObject>
#include <map>
#include "sesion.h"


class Paciente{

public:
    Paciente();
    Paciente(int Id, QString DNI,  QString Name,  QString  Apel, QDateTime Data);//, map<int,Sesion> Sesion);
    Paciente(QJsonObject JsonObject);
    ~Paciente();

    /***Información básica***/
    int idPaciente;
    QString  mDNI, mName, mApel;
    QDateTime mData;
    map<int, Sesion*> sesiones;

    /***ContadoresID***/
    int contSesionID; //Empieza en 1 para que la sesión 0 sea información básica del paciente


};

#endif // PACIENTE_H
