
Pacientes::Player(QObject *parent) : QThread(parent){
  stop = true;
  frameCount = 0;
  time_ms = 0.f;
}

Player::~Player()
{
  mutex.lock();
  stop = true;
  capture.release();
  condition.wakeOne();
  mutex.unlock();
  wait();
}
