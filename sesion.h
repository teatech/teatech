#ifndef SESION_H
#define SESION_H

#include <QDateTime>
#include <map>
#include "prueba.h"

using namespace std;

class Sesion
{
public:
    Sesion();
    Sesion(QJsonObject JsonObject);
    ~Sesion();

    /***Informacion básica***/
    int idSesion, idPaciente;//, idZonaVideo (???);
    QDate fechaSesion;
    map<int,Prueba*> pruebas;

    /***ContadoresID***/
    int contPruebaID; //Empieza en 1 para que la pestaña0 sea lo común de toda la sesión
};

#endif // SESION_H

