#ifndef PRUEBA_H
#define PRUEBA_H

#include <QString>
#include <QJsonObject>
#include <map>
#include "fixation.h"
#include "video.h"

using namespace std;

class Prueba
{
public:
    Prueba();
    Prueba(QJsonObject JsonObject);
    ~Prueba();

    //Parámetros por eventos?
    map<int,float> t_reaccionAEst;
    int idPrueba,idVideo,idSesion;
    Video* video;
    QString archivoTraza;
    QList<Fixation> fixationsList;

    //Parámetros sin zonas
    int num_Fijaciones;
    float t_TotalFijaciones, t_MedioFijaciones, pct_Fijaciones, t_hasta1aFijacion, t_1aFijacion;

    //En los siguienes mapas int sería un ID de zona o evento
    //Parámetros por zonas
    map<int,QList<Fixation>> zone_fixationsList;
    map<int,float> t_hasta1aFijacionXZona, t_1aFijacionXZona;
    map<int, float> pct_FijacionXZona, t_FijacionXZona;
    map<int,int> num_fijacionesXZona;

    QVector<double> vectorX, vectorY;
    //Luego tenemos otros parámetros que en verdad se obtienen de varias pruebas...
    //1.Lo que se mete a MDC de los japos
    //2.El número "real" de zonas de interés

    void fillWithParams(QJsonObject JsonObject);

};

#endif // PRUEBA_H
