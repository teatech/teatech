#ifndef DISPLAYVIDEO_H
#define DISPLAYVIDEO_H

#include "player.h"
#include <QDialog>

namespace Ui {
class Displayvideo;
}

class Displayvideo : public QDialog
{
    Q_OBJECT

public:
    explicit Displayvideo(QWidget *parent = 0);
    ~Displayvideo();
    Player* myPlayerTraza;

private slots:
    void updatePlayerUI(QImage img);
    void PlayVideo(void);

private:
    Ui::Displayvideo *ui;
};

#endif
