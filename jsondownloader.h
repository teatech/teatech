#ifndef JSONDOWNLOADER_H
#define JSONDOWNLOADER_H

#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QString>

#include "paciente.h"
#include "etiqueta.h"


class JSONdownloader : public QObject
{
    Q_OBJECT
public:
    explicit JSONdownloader(QObject *parent = nullptr);
    void retrieveFromDDBB(QString fromTable);
    void retrieveFromDDBB(QString fromTable, int idTarget);
    void retrieveFromDDBB(QString fromTable, QString inRelationWith, int idTarget);
    void sendJSON(Paciente* patient);
    void sendJSON(Sesion* session, QList<Prueba*> tests);
    void sendJSON(Prueba* test);
    void sendJSON(Video* video);
    void sendJSON(Zona* zone);
    void sendJSON(Etiqueta* label);
    void actualizeJSON(Prueba * test);

private:
    //QString url = "http://127.0.0.1:8080/TEAtech-Server/database/";
    QString url = "http://169.254.4.151:8080/TEAtech-Server/database/";
    QNetworkAccessManager *manager = new QNetworkAccessManager(this);
    void sendJSON(QString urlTarget, QByteArray jsonToSend, QString urlEnding);

    Paciente* tempPatient;
    Etiqueta* tempLabel;
    Video* tempVideo;
    Zona* tempZone;

    bool sendingNewInfo = false;
    Sesion* tempSession;
    QList<Prueba*> tempTests;
    QList<Prueba*>::iterator itTests;

signals:
    /**/void patientsReceived(QList<Paciente*> pacients);
    /**/void sessionsFromPatientReceived(Paciente* pacient, QList<Sesion*> sessions);
    /**/void newPatient(Paciente* patient);

    /**/void testFromSessionReceived(/*Sesion* session,*/ QList<Prueba*> tests);
    /**/void newSession(Sesion* session);

    /**/void videosReceived(QList<Video*> videos);
    /**/void zonesFromVideoReceived(Video* video, QList<Zona*> zones);
    /**/void newVideo(Video* video);

    void newZone(Zona* zone);

    /**/void labelsReceived(QList<Etiqueta*> labels);
    /**/void newLabel(Etiqueta* label);

public slots:
    void JSONReceived(QNetworkReply *reply);
};

#endif // JSONDOWNLOADER_H
