#ifndef ROILABEL_H
#define ROILABEL_H

#include <QDialog>
#include <etiqueta.h>


namespace Ui {
class ROILabel;
}

class ROILabel : public QDialog
{
    Q_OBJECT

public:
    explicit ROILabel(QWidget *parent = 0);
    ~ROILabel();
    void setLabelList(QList<Etiqueta*>);
signals:
    void set_event();
    void set_zone();
    void event_or_not(bool isEvent);
    void setLabel(int idEtiqueta);
    void setLabelName(QString label);
private slots:
    void eventOrZone();
private:
    Ui::ROILabel *ui;
};

#endif // ROILABEL_H
