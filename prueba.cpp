#include "prueba.h"
#include <QDebug>


Prueba::Prueba()
{
    idPrueba = 0;
    //***********Inicializaciones**********//
    qDebug() << "Se inicializa";
     num_Fijaciones=t_TotalFijaciones=t_MedioFijaciones=pct_Fijaciones=t_hasta1aFijacion=t_1aFijacion=0;

}
Prueba::Prueba(QJsonObject JsonObject){

    if(JsonObject["id"]!=NULL && JsonObject["id"].toString()!="null")
        idPrueba = JsonObject["id"].toString().toInt();
    else
        idPrueba = -1;

    if(JsonObject["idVideo"]!=NULL && JsonObject["idVideo"].toString()!="null"){
        idVideo = JsonObject["idVideo"].toString().toInt();
    }
    else
        idVideo= -1;

    if(JsonObject["idSession"]!=NULL && JsonObject["idSession"].toString()!="null")
        idSesion = JsonObject["idSession"].toString().toInt();
    else
        idSesion= -1;



    num_Fijaciones=t_TotalFijaciones=t_MedioFijaciones=pct_Fijaciones=t_hasta1aFijacion=t_1aFijacion=0;
    //Aqui puedo poner tambien la ruta a la traza e incluso cargar ya los QVectors de la pupila
}

void Prueba::fillWithParams(QJsonObject JsonObject){

    // Si son los parámetros generales
    if(JsonObject.contains("idZone") && JsonObject["idZone"].toString()=="-1"){

        qDebug("Vamos a rellenar");
        if(JsonObject.contains("numFixations") && JsonObject["numFixations"].toString()!="null"){
            num_Fijaciones = JsonObject["numFixations"].toString().toInt();
        }
        else
            num_Fijaciones= -1;

        if(JsonObject.contains("tTotalFixations") && JsonObject["tTotalFixations"].toString()!="null")
            t_TotalFijaciones = JsonObject["tTotalFixations"].toString().toFloat();
        else
            t_TotalFijaciones = -1;

        if(JsonObject.contains("tAvgFixations") && JsonObject["tAvgFixations"].toString()!="null")
            t_MedioFijaciones = JsonObject["tAvgFixations"].toString().toFloat();
        else
            t_MedioFijaciones= -1;

        if(JsonObject.contains("pctFixations") && JsonObject["pctFixations"].toString()!="null")
            pct_Fijaciones = JsonObject["pctFixations"].toString().toFloat();
        else
            pct_Fijaciones= -1;

        if(JsonObject.contains("tTo1stFixation") && JsonObject["tTo1stFixation"].toString()!="null")
            t_hasta1aFijacion = JsonObject["tTo1stFixation"].toString().toFloat();
        else
            t_hasta1aFijacion= -1;

        if(JsonObject.contains("t1stFixation") && JsonObject["t1stFixation"].toString()!="null")
            t_1aFijacion = JsonObject["t1stFixation"].toString().toFloat();
        else
            t_1aFijacion= -1;
    }

    //Si son los parámetros de zona
    else if(JsonObject.contains("idZone") && JsonObject["idZone"].toString()!="-1"){

        int idZona = JsonObject["idZone"].toString().toInt();

        if(JsonObject.contains("numFixations") && JsonObject["numFixations"].toString()!="null")
            num_fijacionesXZona[idZona] = JsonObject["numFixations"].toString().toInt();

        if(JsonObject.contains("tTotalFixations") && JsonObject["tTotalFixations"].toString()!="null")
            t_FijacionXZona[idZona] = JsonObject["tTotalFixations"].toString().toFloat();

        if(JsonObject.contains("pctFixations") && JsonObject["pctFixations"].toString()!="null")
            pct_FijacionXZona[idZona] = JsonObject["pctFixations"].toString().toFloat();
    }
    qDebug("O mejor no hacemos nada");
}
