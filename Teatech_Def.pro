#-------------------------------------------------
#
# Project created by QtCreator 2018-04-16T18:29:23
#
#-------------------------------------------------

QT       += core gui network printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Teatech_Def
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    etiqueta.cpp \
    fixation.cpp \
    jsondownloader.cpp \
    paciente.cpp \
    player.cpp \
    prueba.cpp \
    qcustomplot.cpp \
    roilabel.cpp \
    sesion.cpp \
    video.cpp \
    zona.cpp

HEADERS += \
        mainwindow.h \
    etiqueta.h \
    fixation.h \
    jsondownloader.h \
    paciente.h \
    player.h \
    prueba.h \
    qcustomplot.h \
    roilabel.h \
    sesion.h \
    video.h \
    zona.h

FORMS += \
        mainwindow.ui \
    displayvideo.ui \
    roilabel.ui


INCLUDEPATH += C:/Users/Elena/Desktop/OpenCV/otra_build/build/install/include

LIBS += C:/Users/Elena/Desktop/OpenCV/build/install/x64/vc15/lib/opencv_world331.lib
