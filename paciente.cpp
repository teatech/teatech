#include "paciente.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"

Paciente::Paciente()
{
    idPaciente = contSesionID = 0;
    mName = mDNI = mApel = "";
    mData = QDateTime::currentDateTime();
//    sesiones = map<int,Sesion>();
}

Paciente::Paciente(int Id, QString DNI,  QString Name,
                            QString  Apel, QDateTime Data){//, map<int,Sesion> Sesion ){
    idPaciente = Id;
    mName = Name;
    mDNI = DNI;
    mApel = Apel;
    mData = Data;
    contSesionID = 0;
//    sesiones = Sesion;
}

Paciente::Paciente(QJsonObject JsonObject){
    if(JsonObject["id"]!=NULL && JsonObject["id"].toString()!="null")
        idPaciente = JsonObject["id"].toString().toInt();
    else
        idPaciente = -1;
    if(JsonObject["dni"]!=NULL /*&& JsonObject["dni"].toString()!="null"*/){
        mDNI = JsonObject["dni"].toString();
    }
    if(JsonObject["name"]!=NULL /*&& JsonObject["name"].toString()!="null"*/){
        mName = JsonObject["name"].toString();
    }
    if(JsonObject["surname"]!=NULL /*&& JsonObject["surname"].toString()!="null"*/){
        mApel = JsonObject["surname"].toString();
    }
    if(JsonObject["surname2"]!=NULL && JsonObject["surname2"].toString()!="null"){
        mApel += " " + JsonObject["surname2"].toString();
        mApel.trimmed();
    }
    if(JsonObject["date"]!=NULL && JsonObject["date"].toString()!="null"){
        QDate auxDate;
        auxDate.fromString(JsonObject["date"].toString(), "yyyy-MM-dd");
        QDateTime myDate(auxDate);
        mData = myDate;
    }

}

Paciente::~Paciente()
{

}
