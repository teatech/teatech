#ifndef FIXATION_H
#define FIXATION_H

#include <opencv2/core/core.hpp>
#include <QJsonObject>

//using namespace cv;

class Fixation
{
public:
    Fixation();
    Fixation(int x, int y);
    Fixation(QJsonObject jsonReceived);
    int frame, duration;
    cv::Point coord;

};

#endif // FIXATION_H
