#-------------------------------------------------
#
# Project created by QtCreator 2018-02-26T17:45:46
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Teatech
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    player.cpp \
    paciente.cpp \
    sesion.cpp \
    prueba.cpp \
    displayvideo.cpp

HEADERS += \
        mainwindow.h \
    player.h \
    paciente.h \
    sesion.h \
    prueba.h \
    displayvideo.h

FORMS += \
        mainwindow.ui \
    displayvideo.ui


INCLUDEPATH += C:/opencv3/build/install/include

LIBS += C:/opencv3/build/install/x64/vc15/lib/opencv_world341d.lib
