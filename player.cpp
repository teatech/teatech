#include "player.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"



Player::Player(QObject *parent) : QThread(parent){
    qDebug() << "Iniciando myPlayer";
    stop = first = true;
    mostrarCam = errorCam = mostrarPupila = false;
    frameCount =  fixationThresh_frames = timeFirstFixation = delay = 0;
    fixationThresh_ms = 300;
    time_ms = 0.f;
    videoID = 0;
    mode = 0;
    bbox = Rect2d(287, 23, 86, 320);
    tracker = TrackerKCF::create();
    gazePlotData = {};
    label = "-";
}


void Player::Stop()
{
    stop = true;
}

bool Player::isStopped() const{
    return this->stop;
}

bool Player::loadVideo(Video *myVideo){

    qDebug() << " load Video";
    qDebug(QString::number(myVideo->idVideo).toUtf8());
    currentVideo = myVideo;
    delay = (1000/currentVideo->fps);
    frameCount = 0;
    qDebug() << " load Video";
    QString nameVideo = "Data/Videos/";
    nameVideo+=QString::number(currentVideo->idVideo);
    nameVideo+="/";
    nameVideo+=currentVideo->nombreVideo;
    qDebug(nameVideo.toUtf8());
    capture.open(nameVideo.toUtf8().constData());
    if (capture.isOpened()){
        qDebug() << "He conseguido abrir " << currentVideo->nombreVideo;
        return true;
    }
    qDebug() << "No se ha podido abrir el vídeo";
    return false;
}

void Player::Play(){
    if (isStopped()){
        stop = false;
    }
    qDebug() << "Voy a empezar el thread";
    this->thread()->start(LowPriority);
    emit go_video();
    qDebug() << "Está";
//    this->start(LowPriority);
}

void Player::reproduce(){
    qDebug() << "Voy a reproducir " << currentVideo->nombreVideo;
        fixationThresh_frames = fixationThresh_ms / delay;
        Zona *auxZona = new Zona();
         qDebug() << "Vuelvo empezar";
        //Mode seguimiento de zona
        if( (mode == 2 && frameCount == 0) || (mode == 4) ){
            mode = 5;
            auxZona->idZona = currentVideo->contIdZona;
            auxZona->frameStart = frameCount;
            auxZona->label = label;
            currentVideo->contIdZona++;
            capture.read(frame);
            bbox = selectROI(frame, false);
            tracker->init(frame, bbox);
            cvDestroyAllWindows();
        }
        qDebug() << "Mode is " << mode;
        while(capture.read(frame)){
            if(stop)  return;
            frameCount++;
            time_ms += delay;

            if(!mode || mode == 3 || mode == 8){
                QList<map<int,Zona*>> listMaps;
                listMaps.push_back(currentVideo->zonas);
                listMaps.push_back(currentVideo->zonasReales);

                for(int i = 0; i < 2; i++){
                    for(auto it = listMaps.at(i).begin(); it != listMaps.at(i).end(); it++){
                        Zona *auxZona2;
                        QList<Point> auxPoints;
                        auxZona2 = it->second;
                        if(auxZona2->zonasMap.count(frameCount) == 0){
                            qDebug() << frameCount;
                            continue;
                        }
                        auxPoints = auxZona2->zonasMap.at(frameCount);
                        auto p1 = auxPoints.begin();
                        auto p2 = auxPoints.begin();
                        p2++;
                        rectangle(frame,*p1,*p2,Scalar(255,0,0),2);
                    }
                }
            }

            if(mode == 7 || mode == 8){
                SimpleGazePlot(frame, frameCount);
            }
            if(mode == 1 || mode == 3){
                GazePlot(frame, frameCount);
            }
            if(mode == 2 || mode == 5){
                if(tracker->update(frame, bbox)) rectangle(frame, bbox, Scalar(255, 0, 0), 2, 1);
                QList<Point> auxList;
                auxList.push_back(Point(bbox.x,bbox.y));
                auxList.push_back((Point(bbox.x + bbox.width,bbox.y + bbox.height)));
                auxZones[frameCount] = auxList;
            }
            if(mode == 6){
                auxZona->frameEnd = frameCount;
            }

            if(mode != 5) QThread::msleep(ulong(delay));

            if(mostrarCam && !errorCam){
                Mat camFrame;
                namedWindow("WebCam", WINDOW_NORMAL);
                cam.read(camFrame);
                resizeWindow("WebCam",camFrame.cols/3,camFrame.rows/3);
                imshow("WebCam", camFrame);
                if(first) moveWindow("WebCam",30,300);
                first = false;
                qApp->processEvents();
            }
            string tamPupila = "-";
            if(pupilData.count(frameCount)){
                stringstream ss;
                ss << fixed << setprecision(2) << pupilData.at(frameCount);
                tamPupila = ss.str();
            }
            if(mostrarPupila) putText(frame,"Tamaño de pupila: "  + tamPupila, Point(50, 50), FONT_HERSHEY_COMPLEX_SMALL, 1, Scalar(0, 0,0));
            imshow_u(frame);
        }

        if(mode == 2 || mode == 6 || mode == 5){
    //        qDebug() << "Voy a guardar el video";
            if(auxZona->frameEnd == 0)
                auxZona->frameEnd = frameCount;
            auxZona->zonasMap = auxZones;
            currentVideo->zonas[currentVideo->contIdZona] = auxZona;
            //Escribir txt de la zona
            QString comando = "Data/Videos/" + QString::number(currentVideo->idVideo);
            qDebug() << comando;

            if (QDir().mkpath(comando)){
                qDebug() << "Funciona";
            }
            comando += "/zona_" + QString::number(currentVideo->contIdZona) + ".txt";
            ofstream fs(comando.toUtf8().constData());
            for (auto it=auxZones.begin() ; it!=auxZones.end() ; ++it){
                  // Enviamos una cadena al fichero de salida:
                  fs << it->first << " " << it->second.at(0).x << " " << it->second.at(0).y
                     << " " << it->second.at(1).x << " " << it->second.at(1).y << endl;
            }
            fs.close();
            currentVideo->contIdZona++;

            for(auto del = auxZones.begin(); del != auxZones.end(); del++)
                del->second.clear();
            auxZones.clear();

        }
        first = true;
        destroyAllWindows();
        emit finished();
    //    resetPlayer();
}

void Player::SimpleGazePlot(Mat img, int frameCount){
    /****Parameters****/
    Scalar color = Scalar(0, 0, 255);
    int pointMinSize = 9, pointMaxScale = 5, labelOffset = -4, number = 1;
    float pointIncrement = 0.06f;
    for(auto f = currentPrueba->fixationsList.begin(); f != currentPrueba->fixationsList.end(); f++){
        if(frameCount >= f->frame  && frameCount <= f->frame+f->duration){
            qDebug() << "Frame " << frameCount << " punto (" << f->coord.x << ", " << f->coord.y << ")";
            previousPoints.push_back(f->coord);
        }
    }
    if(previousPoints.empty())
        return;


    previousPoint = currentPoint = previousPoints.first();

    for (auto it = previousPoints.begin(); it != previousPoints.end(); it++) {
        if(*it == currentPoint) {
                pointScaleFactor += pointIncrement; if (pointScaleFactor >= pointMaxScale) pointScaleFactor = pointMaxScale;
               circle(img, currentPoint, pointMinSize*pointScaleFactor, color, -1);
               putText(img, to_string(number), Point(currentPoint.x - labelOffset, currentPoint.y - labelOffset), FONT_HERSHEY_COMPLEX_SMALL, .8, Scalar(0, 0, 0));
        }
        else {
            number++;
            previousPoint = currentPoint;
            currentPoint = *it;
            line(img, previousPoint, currentPoint, color, 1);
            pointScaleFactor = 1;
            auxFixations = 0;
        }
    }
    pointScaleFactor = 1;
}

void Player::GazePlot(Mat img, int frameCount) {
    /****Parameters****/
    Scalar color = Scalar(0, 0, 255);
    int pointMinSize = 9, pointMaxScale = 5, labelOffset = -4;
    float pointIncrement = 0.06f;
    int numPreviousPoints = 9999;
    int frames_00 = 0;

    int pointNumber = 0;
    if (gazePlotData.count(frameCount) != 0){
        if(gazePlotData.at(frameCount).x <= 0 ||  gazePlotData.at(frameCount).y <= 0){
            frames_00++;
        }
        else
            previousPoints.push_back(gazePlotData.at(frameCount));
    }
    if(previousPoints.empty()) return;
    fixationsTime.clear();
    if (previousPoints.size() == numPreviousPoints)
        previousPoints.pop_front();

    auto it = previousPoints.begin();
    previousPoint = currentPoint = previousPoints.first();
    auxFixations = 0;
    //it++;
    for (it; it != previousPoints.end(); it++) {
        if (dist(currentPoint, *it) == 0)
            continue;
        if (dist(currentPoint, *it) < distSamePoint) {
            if (auxFixations < fixationThresh_frames)
                auxFixations++;
            else {
                if (auxFixations == fixationThresh_frames) {
                    pointNumber++;
                    auxFixations++;
                }
                pointScaleFactor += pointIncrement; if (pointScaleFactor >= pointMaxScale) pointScaleFactor = pointMaxScale;
                circle(img, currentPoint, pointMinSize*pointScaleFactor, color, -1);
                putText(img, to_string(pointNumber), Point(currentPoint.x - labelOffset, currentPoint.y - labelOffset), FONT_HERSHEY_COMPLEX_SMALL, .8, Scalar(0, 0, 0));
            }
        }
        else {
            previousPoint = currentPoint;
            currentPoint = *it;
            pointScaleFactor = 1;
            auxFixations = 0;
        }
        line(img, previousPoint, currentPoint, color, 1);
    }

}

float Player::dist(Point a, Point b) {
    Point2f i((float)a.x, (float)a.y);
    Point2f j((float)b.x, (float)b.y);

    float x = i.x - j.x;
    float y = i.y - j.y;

    return x*x + y*y;
}

void Player::setCurrentPrueba(Prueba *prueba){
    currentPrueba = prueba;
}

void Player::setMode(int i) {
    mode = i;
}

void Player::getFixList(QList<Fixation> fix){
    fixList = fix;
}

void Player::setGazePlotFile(QString gazeName){
    qDebug() << "Voy a dibujar la traza de " << gazeName;
    gazePlotData = readData(gazeName.toUtf8().constData());
    QStringList split = gazeName.split("/");
    split.pop_back();
    QString pupil = split.join("/") + "/pupila.txt";
    pupilData = readPupil(pupil.toUtf8().constData());
    qDebug() << "Read pupil" << endl;
    QString camFile = split.join("/") + "/cam.avi";
    qDebug() << "Camfile: " << camFile;
    cam.open(camFile.toUtf8().constData());
    if (cam.isOpened()){
        qDebug() << "He conseguido abrir " << camFile;
         errorCam = false;
    }else{
        qDebug() << "No se ha podido abrir el vídeo de la webcam ";
        errorCam = true;
    }
}

map<int, cv::Point> Player::readData(String filename) {
    ifstream f;
    f.open(filename,ios::in);
    map<int, Point> auxMap;
    if(f.is_open()){
        while (!f.eof()) {
            int x, y, frame;
            f >> frame >> x >> y;
            auxMap.insert(pair<int, Point>(frame, Point(x, y)));
        }
        f.close();
        return auxMap;
    }
    else{
        qDebug() << "No se ha podido leer el fichero de traza.";
        auxMap.clear();
        return auxMap;
    }

}

//Envia un Mat al display del gui
void Player::imshow_u(Mat frame){
    //    Mat RGBframe;
    cvtColor(frame, RGBframe, CV_BGR2RGB);
    //QImage::Fo
    img = QImage((const unsigned char*)(RGBframe.data), RGBframe.cols,RGBframe.rows,QImage::Format_RGB888);
    emit processedImage(img);
}

map<int,float> Player::readPupil(String filename){
    qDebug() << "Gona read pupil";
    ifstream f;
    f.open(filename,ios::in);
    map<int, float> auxMap;
    if(f.is_open()){
        while (!f.eof()) {
            float t;
            float size;
            f >> t >> size;
           // qDebug() << "t is " << t << " and delay is " << delay << endl;
            int frame = 1000*t/delay;
            auxMap.insert(pair<int, float>(frame, size));
            //qDebug() << "Frame: " << frame << ", size: " << size;
        }
        f.close();
        return auxMap;
    }
    else{
        qDebug() << "No se ha podido leer el fichero de pupila.";
        auxMap.clear();
        return auxMap;
    }
}

Player::~Player()
{
    qDebug() << "Destruyendo Player";
    //tracker = NULL;
    mutex.lock();
    stop = true;
    capture.release();
    condition.wakeOne();
    mutex.unlock();
    wait();

}


