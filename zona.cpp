#include "zona.h"

Zona::Zona()
{

}
Zona::Zona(QJsonObject JsonObject){

    if(JsonObject["id"]!=NULL && JsonObject["id"].toString()!="null")
        idZona = JsonObject["id"].toString().toInt();
    else
        idZona = -1;

    if(JsonObject["idVideo"]!=NULL && JsonObject["idVideo"].toString()!="null")
        idVideo = JsonObject["idVideo"].toString().toInt();
    else
        idVideo = -1;

    if(JsonObject["idLabel"]!=NULL && JsonObject["idLabel"].toString()!="null")
        idEtiqueta = JsonObject["idLabel"].toString().toInt();
    else
        idEtiqueta = -1;

}


bool Zona::isFixIn(Fixation fix){
    if(zonasMap.count(fix.frame)){
        QList<cv::Point> vertices = zonasMap.at(fix.frame);
        qDebug() << "Is point (" << fix.coord.x << ", " << fix.coord.y << ")...";
        int smallX = vertices.at(0).x;
        int smallY = vertices.at(0).y;
        int bigX = vertices.at(1).x;
        int bigY = vertices.at(1).y;
        qDebug() << "...inside of zone delimited by big(" << bigX << "," << bigY << ")"
                 << " and small(" << smallX << "," << smallY << ")";

        if ((fix.coord.x > smallX && fix.coord.x < bigX) && (fix.coord.y > smallY && fix.coord.y < bigY))
            return true;
    }
    return false;
}
