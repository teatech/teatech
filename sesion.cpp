#include "sesion.h"

Sesion::Sesion()
{
    idSesion = 0;
    contPruebaID  = 1;
}
Sesion::Sesion(QJsonObject JsonObject){

    if(JsonObject["id"]!=NULL && JsonObject["id"].toString()!="null"){
        idSesion = JsonObject["id"].toString().toInt();
    }
    else
        idSesion = -1;

    if(JsonObject["idPatient"]!=NULL && JsonObject["idPatient"].toString()!="null"){
        idPaciente = JsonObject["idPatient"].toString().toInt();
    }
    else
        idPaciente = -1;

    if(JsonObject["date"]!=NULL && JsonObject["date"].toString()!="null"){
        QDate auxDate;
        auxDate.fromString(JsonObject["date"].toString(), "yyyy-MM-dd");
        fechaSesion = auxDate;
     }
}
