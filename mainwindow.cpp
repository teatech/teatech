#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <roilabel.h>
#include <QDebug>
#include <QDialog>
#include <QVBoxLayout>
#include <QLayoutItem>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{   
    //**********Inicializaciones*********//
    ui->setupUi(this);
    ui->TB_Pruebas->removeTab(1); //Eliminar tab a mayores que está creada por defecto
    pacientes = {};
    contPacienteID = contVideoID = mode = 0;
    label = "-";
    currentPrueba = new Prueba();
   // Prueba *pr = new Prueba();
   // Sesion *ss = new Sesion();
   // getParameters(pr,ss);
   // waitKey(0);

    jsonDownloader = new JSONdownloader();

    connect(jsonDownloader, SIGNAL(patientsReceived(QList<Paciente*>)), this, SLOT(readPatients(QList<Paciente*>)));
    connect(jsonDownloader, SIGNAL(sessionsFromPatientReceived(Paciente*, QList<Sesion*>)), this, SLOT(readPatientInfo(Paciente*, QList<Sesion*>)));
    connect(jsonDownloader, SIGNAL(testFromSessionReceived(/*Sesion*,*/QList<Prueba*>)), this, SLOT(readSessionInfo(/*Sesion*,*/QList<Prueba*>)));
    connect(jsonDownloader, SIGNAL(videosReceived(QList<Video*>)), this, SLOT(readVideos(QList<Video*>)));
    connect(jsonDownloader, SIGNAL(zonesFromVideoReceived(Video*, QList<Zona*>)), this, SLOT(readVideoInfo(Video*, QList<Zona*>)));
    connect(jsonDownloader, SIGNAL(labelsReceived(QList<Etiqueta*>)), this, SLOT(readLabels(QList<Etiqueta*>)));

    // Tras haber creado un nuevo objeto
    connect(jsonDownloader, SIGNAL(newPatient(Paciente*)), this, SLOT(readNewPatient(Paciente*)));
    connect(jsonDownloader, SIGNAL(newSession(Sesion*)), this, SLOT(GoSessionFinish(Sesion*)));
    connect(jsonDownloader, SIGNAL(newVideo(Video*)), this, SLOT(readNewVideo(Video*)));
    connect(jsonDownloader, SIGNAL(newLabel(Etiqueta*)), this, SLOT(readNewLabel(Etiqueta*)));
    connect(jsonDownloader, SIGNAL(newZone(Zona*)), this, SLOT(readNewZone(Zona*)));

    //AÑADIR LISTAS PRINCIPALES A LA BASE DE DATOS
    jsonDownloader->retrieveFromDDBB("patients");
    jsonDownloader->retrieveFromDDBB("labels");
    jsonDownloader->retrieveFromDDBB("videos");

    // CONEXIONES de nuestros objetos a nuestros slots
    connect(ui->BAddVideo,SIGNAL(clicked()),this,SLOT(AddVideo()));
    connect(ui->BAddPaciente,SIGNAL(clicked()),this,SLOT(AddPaciente()));
    connect(ui->BShowVideos,SIGNAL(clicked()),this,SLOT(ShowVideos()));
    connect(ui->BShowPacientes,SIGNAL(clicked()),this,SLOT(ShowPacientes()));
    connect(ui->BSubmitPaciente,SIGNAL(clicked()),this,SLOT(SubmitPaciente()));
    connect(ui->BPlay,SIGNAL(clicked()),this,SLOT(PlayVideo()));
    connect(ui->BCalibrar,SIGNAL(clicked()),this,SLOT(Calibrar()));
    connect(ui->BStartSesion,SIGNAL(clicked()),this,SLOT(StartSesion()));
    connect(ui->BplayVideo,SIGNAL(clicked()),this,SLOT(PlayVideoSelected()));
    connect(ui->BGoSesion,SIGNAL(clicked()),this,SLOT(GoSesion()));
    connect(ui->BAddZona,SIGNAL(clicked()),this,SLOT(SelectROILabel()));

    connect(ui->BEvent, SIGNAL(clicked()), this, SLOT(StartEndEvent()));

//    connect(myPlayer, SIGNAL(finished()), this, SLOT(quit()));
//    connect(myPlayer, SIGNAL(finished()), myPlayer, SLOT(deleteLater()));
//    connect(this, SIGNAL(finished()), this, SLOT(deleteLater()));

    if( ui->stackedWidget_2->currentIndex() != 2) ui->stackedWidget_2->setCurrentIndex(2);

}


//void MainWindow::ShowHistorialSlot(){ ShowHistorial(pacienteSlot);}

void MainWindow::ShowPacientes(void)
{
    if( ui->stackedWidget->currentIndex() != 2) ui->stackedWidget->setCurrentIndex(2);
    //if( ui->stackedWidget_2->currentIndex() != 0) ui->stackedWidget_2->setCurrentIndex(0);
    if( ui->stackedWidget_2->currentIndex() != 4) ui->stackedWidget_2->setCurrentIndex(4);

    clearLayout(ui->VLPacientes);
    for (auto it=pacientes.begin(); it != pacientes.end(); ++it){
        int id = it->second->idPaciente;
        //qDebug() << "it: " << id <<" name " + it->second->mName;
        QString btName = "Pac_" + QString::number(id);
        QHBoxLayout *Hlayout = new QHBoxLayout;
        QPushButton *BPaciente = new QPushButton(btName);
        QLabel *LPaciente = new QLabel(it->second->mName + " " + it->second->mApel);
        connect(BPaciente, SIGNAL(clicked()), this, SLOT(ShowHistorial()));
        Hlayout->addWidget(BPaciente);
        Hlayout->addWidget(LPaciente);
        ui->VLPacientes->addLayout(Hlayout);
    }
}

void MainWindow::ShowHistorial()
{
    if( ui->stackedWidget->currentIndex() != 3) ui->stackedWidget->setCurrentIndex(3);
    if( ui->stackedWidget_2->currentIndex() != 1) ui->stackedWidget_2->setCurrentIndex(1);

    //Detectar botón pulsado
    QPushButton* buttonSender = qobject_cast<QPushButton*>(sender());
    QString buttonText = buttonSender->text();
    //Obtener paciente corespondiente
    Paciente *paciente = pacientes.at(splitID(buttonText));
    currentPacient = paciente;
    ui->LName->setText(paciente->mName);

    if(paciente->sesiones.empty()){
        jsonDownloader->retrieveFromDDBB("patients", paciente->idPaciente);
    }
    else{
        displaySessions(paciente);
    }
}

void MainWindow::ShowPruebas(){

    if (currentPacient->sesiones.empty()){
        ui->LnameHisto->setText("No hay ninguna sesión seleccionada.");
        ui->LdateHisto->setText("");
        ui->LfixPctHisto->setText("");
    }
    else{
        ui->LnameHisto->setText(" ");
        ui->LdateHisto->setText("Fecha: ");
        ui->LfixPctHisto->setText(" ");
    }

    if(ui->stackedWidget->currentIndex() != 3) ui->stackedWidget->setCurrentIndex(3);
    //Detectar botón pulsado
    QPushButton* buttonSender = qobject_cast<QPushButton*>(sender());
    QString buttonText = buttonSender->text();

    //Acceder a la sesion seleccionada
    Sesion *sesion = currentPacient->sesiones.at(splitID(buttonText));
    currentSesion = sesion;
    map<int,Prueba*> auxMap = sesion->pruebas;

    //Rellenamos informacion General de la Sesion (TAB 0)
    ui->L_GazeFilename->setText(sesion->fechaSesion.toString("dd.MM.yyyy"));

    //Borramos TABS que hubiera antes
    //Excepto el 0 que es de info General
    int numTabs = ui->TB_Pruebas->count();
    ui->TB_Pruebas->setCurrentIndex(0);
    for (int i=1; i<numTabs; i++){
        ui->TB_Pruebas->removeTab(1);
}

    //Crea un tab por cada prueba, empezando por prueba_1
    if(sesion->pruebas.empty()){
        qDebug() << "Esta vacio pruebas";
        jsonDownloader->retrieveFromDDBB("sessions", sesion->idSesion);
        qDebug() << "salio de retrieve";
     }

     else displayTests(sesion->pruebas);


}

void MainWindow::FullGaze(void){
    QList<QString> lista;
    for (int i=0; i<12; i++){
        lista.append("traza"+QString::number(i)+".txt ");
    }
    currentPrueba = currentSesion->pruebas.at(ui->TB_Pruebas->currentIndex());

    QString miTraza = "Data/Pacientes/" + QString::number(currentPacient->idPaciente) + "/" +
                        QString::number(currentSesion->idSesion) +"/"+ QString::number(currentPrueba->idPrueba) +
                        "/prueba.txt";
    lista.append(miTraza);

    QString comando = "python GazePatternAnalysis.py ";
    for (auto it = lista.begin(); it != lista.end(); it++){
        comando += *it + " ";
    }
    qDebug() << comando;
    system(comando.toUtf8().constData());

    ifstream f;
    f.open("FullGazePattern.txt",ios::in);
    QList<Point> auxList;
    if(f.is_open()){
        while (!f.eof()) {
            int x, y;
            f >> x >> y;
            auxList.push_back(Point(x, y));
        }
        f.close();
    }
    else{
        qDebug() << "No se ha podido leer el fichero del análisis de la traza.";
        auxList.clear();
    }
    


}

void MainWindow::ShowVideos(void)
{
    clearLayout(ui->LvideosIzq);
    qDebug() << "EN SHOW VIDEOS";

    //Cambios de vistas de Interfaz correspondientes
    if( ui->stackedWidget->currentIndex() != 0) ui->stackedWidget->setCurrentIndex(0);
    if( ui->stackedWidget_2->currentIndex() != 2) ui->stackedWidget_2->setCurrentIndex(2);
    if( ui->stackedWidget_3->currentIndex() != 0) ui->stackedWidget_3->setCurrentIndex(0);

    //Crea botones columna menu Izquierda
    for(auto it =  videos.begin(); it != videos.end(); it++){
        qDebug() << "Voy a abrir el archivo " << it->second->nombreVideo.toUtf8().constData();
        qDebug() << "Que tiene el id... " << it->second->idVideo;
        QPushButton *auxBT = new QPushButton("Video_" + QString::number(it->second->idVideo));
        connect(auxBT, SIGNAL(clicked()), this, SLOT(ShowSelectedVideo()));
        ui->LvideosIzq->addWidget(auxBT);

    }
}

void MainWindow::PlayVideoSelected(){
    qDebug() << "Voy a mostar el video...";
    VideoCapture c;
    Mat frame;
    map<int,Zona*> zonas;
    map<int,Zona*> zonasReales;
    map<int,QList<Point>> framesZona;
    //int framecount = 0;

    //myPlayer->setMode(0);
    zonas = currentVideo->zonas;
    //si todavía no se han cargado las zonas del video se leen del txt y se guardan
    if (ui->CBzona_marcadas->isChecked()){
        if (zonas.empty()){
            Zona* zonaux = new Zona();
            QList<Point> listaux;
            int frame, x1, x2, y1, y2;
            //AÑADIR LLAMADA BASE DE DATOS CON LOS NOMBRES DE ZONA
            //IF LA LLAMADA A LA BD DICE QUE NO HAY ZONAS
            // QMessageBox msgBox;
            //msgBox.setText("No tienes zonas marcadas");
            //msgBox.exec();
            //ELSE
            ifstream fe("zona.txt");
            fe >> frame >> x1 >> y1 >> x2 >> y2;
            listaux.append(Point(x1,y1));
            listaux.append(Point(x2,y2));
            zonaux->zonasMap[frame] = listaux;
            //zonaux->idVideo = currentVideo->idVideo;
            //currentVideo->zonas[currentVideo->contIdZona] = zonaux;
            //currentVideo->contIdZona++;
        }
    }
    if (ui->CB_zonas_identificadas->isChecked()){
        //LLAMADA FUNCION ZONAS REALES DE INTERES
        //llamada a BD que devuelva todos los objetos fijacion, de un video determinado
        QList<Fixation> auxFix;

            auxFix.push_back(Fixation(125,50));
            auxFix.push_back(Fixation(187,50));
            auxFix.push_back(Fixation(125,75));
            auxFix.push_back(Fixation(171,58));

        getRealROI(auxFix);
    }
    else{
        zonasReales.clear();
    }
    qDebug() << "Que se llama " << currentVideo->nombreVideo;

    QString ruta_vid = "Data/Videos/" + QString::number(currentVideo->idVideo) + "/" + currentVideo->nombreVideo;
    c.open(ruta_vid.toUtf8().constData());
    if(!c.isOpened()){
        QMessageBox msgBox;
        msgBox.setText("No se ha podido abrir aquí el vídeo");
        msgBox.exec();
        return;
    }

    /***Poner la reproiducción del vídeo en un thread***/
    createPlayerThread(0);

}
void MainWindow::getRealROI(QList<Fixation> auxFix){

    QList<Point> fix;
    QList<QList<Point>> zonas_reales;
    int valor = 45; //default
    bool fin = false;

    VideoCapture c(currentVideo->nombreVideo.toUtf8().constData());
    valor = sqrt(c.get(CV_CAP_PROP_FRAME_WIDTH)*c.get(CV_CAP_PROP_FRAME_WIDTH)+c.get(CV_CAP_PROP_FRAME_HEIGHT)*c.get(CV_CAP_PROP_FRAME_HEIGHT))*0.0742; //default value
    qDebug() << "VALOR EMPIRICO" << valor;

    for(auto it = auxFix.begin(); it != auxFix.end(); it++)
        fix.append(it->coord);

    for (auto pfix = fix.begin(); pfix != fix.end(); ++pfix) {
        if (!fin){
            QList<Point> aux;
            aux.push_back(*pfix);
            zonas_reales.push_back(aux);
        }

        fin = false;

        for (auto lpuntos = zonas_reales.begin(); lpuntos != zonas_reales.end(); ++lpuntos) {
             for (auto punto = lpuntos->begin(); punto != lpuntos->end(); ++punto) {

                 int dist_puntos = (pfix->x - punto->x) * (pfix->x - punto->x) + (pfix->y - punto->y) * (pfix->y - punto->y);
                 if (dist_puntos < valor*valor){
                     lpuntos->push_back(*pfix);
                     fin = true;
                     break;
                 }
             }
             if (fin) break;
        }
   }

   float n = zonas_reales.size(); //numero de zonas a priori
   float multi_coef = 1; //pnt_z1 * pnt_z2 * pnt_z3 * ... * pnt_zn -> interior de la raiz n de la media geometrica

   for (auto lpuntos = zonas_reales.begin(); lpuntos != zonas_reales.end(); ++lpuntos) {
        multi_coef *= lpuntos->size();
   }

   float avgGeo = pow(multi_coef,1/n);

   auto it = zonas_reales.begin();
   while (it != zonas_reales.end()) {
     if (it->size() < avgGeo)
       it = zonas_reales.erase(it);
     else
       ++it;
   }

   int rrIndex = 0;
   for (auto lpuntos = zonas_reales.begin(); lpuntos != zonas_reales.end(); ++lpuntos) {
       vector<Point> v{begin(*lpuntos), end(*lpuntos) };

       Zona *auxZona = new Zona();
       map<int,QList<Point>> auxMap;

       auxZona->idZona = rrIndex++;
       Rect rect = boundingRect(v);

       QList<Point> auxList;
       auxList.append(Point(rect.x-10,rect.y-10));
       auxList.append(Point(rect.x+rect.width+10,rect.y+rect.height+10));

       for(int i = 0; i < c.get(CAP_PROP_FRAME_COUNT);i++)
           auxMap[i] = auxList;

       auxZona->zonasMap = auxMap;
       currentVideo->zonasReales[auxZona->idZona] = auxZona;
   }
   return;
}
void MainWindow::createPlayerThread(int mode){
    QThread* thread = new QThread;
    myPlayer = new Player();

    myPlayer->moveToThread(thread);

    connect(myPlayer, SIGNAL(go_video()), myPlayer, SLOT(reproduce()));
    // connect(myPlayer, SIGNAL(finished()), this, SLOT(updateVideo()));
    connect(myPlayer, SIGNAL(processedImage(QImage)),this, SLOT(updatePlayerUI(QImage)));
    connect(myPlayer, SIGNAL(finished()), this, SLOT(resetPlayer()));
    connect(myPlayer, SIGNAL(finished()), myPlayer, SLOT(deleteLater()));
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));


    myPlayer->setCurrentPrueba(currentPrueba);
    qDebug() << "Carga current prueba cacia";
    myPlayer->loadVideo(currentVideo);
    myPlayer->fixationThresh_ms = ui->SFixTime->value();
    myPlayer->distSamePoint = ui->SFixRad->value();
    qDebug() << "Mode is " << mode;
    myPlayer->setMode(mode);
    myPlayer->label = label;
    if(mode == 1 || mode == 3){
        QString ficheroTraza = "Data/Pacientes/" + QString::number(currentPacient->idPaciente) + "/" +
                                QString::number(currentSesion->idSesion) + "/" + QString::number(currentPrueba->idPrueba)
                                + "/prueba.txt";

        myPlayer->setGazePlotFile(ficheroTraza);
    }
    if( ui->stackedWidget->currentIndex() != 4) ui->stackedWidget->setCurrentIndex(4);
    return;
}

void MainWindow::ShowSelectedVideo(){

    //Detectar botón pulsado
    QPushButton* buttonSender = qobject_cast<QPushButton*>(sender());
    QString buttonText = buttonSender->text();
    currentVideo = videos.at(splitID(buttonText));

    if( ui->stackedWidget_3->currentIndex() != 1) ui->stackedWidget_3->setCurrentIndex(1);
    ui->LnameVideo->setText(currentVideo->nombreVideo);

    if(currentVideo->zonas.empty())
        jsonDownloader->retrieveFromDDBB("videos", currentVideo->idVideo);
}

//Añade el video a la lista de Videos cargados
void MainWindow::AddVideo(void)
{    
    QString filename = QFileDialog::getOpenFileName(this,tr("Open Video"), ".",tr("Video Files (*.avi *.mpg *.mp4)"));    
    qDebug() << "Guardar un video " << filename;

    if (!filename.isEmpty()){
        VideoCapture c;
        c.open(filename.toUtf8().constData());
        if(c.isOpened()){
            QStringList split = filename.split("/");

            Video *auxVideo = new Video();
            auxVideo->fps = (int) c.get(CV_CAP_PROP_FPS);
            auxVideo->idVideo = contVideoID++;
            auxVideo->nombreVideo = split[split.length()-1];
            auxVideo->originalFileName = filename;

            jsonDownloader->sendJSON(auxVideo);

            //videos[auxVideo->idVideo] = auxVideo;
            //ShowVideos();
            return;
        }
    }
    else{
        QMessageBox msgBox;
        msgBox.setText("Error creando Video");
        msgBox.exec();
    }
}

void MainWindow::AddPaciente(void)
{
    if( ui->stackedWidget->currentIndex() != 1) ui->stackedWidget->setCurrentIndex(1);

}

void MainWindow::Calibrar(void)
{
    //llamada a la funcion de calibrar de Elena
    system("calibracion.exe");

}

void MainWindow::StartSesion(void)
{

    clearLayout(ui->VL_VideoSesion);
    qDebug() << "En añadir CheckBox.  Lista vídeos tamaño: " << videos.size();

    if(ui->stackedWidget->currentIndex() != 5) ui->stackedWidget->setCurrentIndex(5);
    for(auto it = videos.begin(); it != videos.end(); it++){

        QString nombreCB = it->second->nombreVideo + "_" + QString::number(it->first);
        qDebug() << "añadiendo : " << nombreCB;
        QCheckBox *cbvideo = new QCheckBox(nombreCB, this);
        ui->VL_VideoSesion->addWidget(cbvideo);
    }
}

void MainWindow::GoSesion(void){
    //LLama a reproducir esas pruebas

    QList<Video*> videoSesion;
    int numCB = ui->VL_VideoSesion->count();
    qDebug() << "Num cb " << numCB;
    for( int i = 0; i<numCB; i++){
        qDebug() << " En bucle for. Take at " << i;
        QLayoutItem *item = ui->VL_VideoSesion->itemAt(i);
        if(item->widget()){
            qDebug() << "El item es un widget";
            QCheckBox *q = (QCheckBox *)item->widget();
            qDebug() << "Lo he casteado a checkbox";
            if (q->isChecked()){
                qDebug() << q->text();
                videoSesion.push_back( videos.at(splitID(q->text())) );
                qDebug() << "Esta marcado" << q->text();
            }
        }
    }

    //comprobacion alguno marcado
    qDebug() << "Numero de videos marcados" << videoSesion.size();
    if(!videoSesion.size()) return;

    Sesion *auxSesion = new Sesion();
    auxSesion->idPaciente = currentPacient->idPaciente;
    auxSesion->fechaSesion = QDate::currentDate();

    QList<Prueba*> tests;

    for(auto it = videoSesion.begin(); it != videoSesion.end(); it++){
        Prueba *auxPrueba = new Prueba();
        auxPrueba->idPrueba = 0;
        auxPrueba->video = *it;
        auxPrueba->idVideo = (*it)->idVideo;

        tests.append(auxPrueba);
    }

    //currentPacient->sesiones[auxSesion->idSesion] = auxSesion;
    jsonDownloader->sendJSON(auxSesion, tests);
}

void MainWindow::GoSessionFinish(Sesion* session){

    currentPacient->sesiones[session->idSesion] = session;
    currentSesion=session;

    for(auto it = session->pruebas.begin(); it != session->pruebas.end(); it++){

        QString comando = "Data/Pacientes/" + QString::number(currentPacient->idPaciente) + "/" +
                           QString::number(session->idSesion) + "/" + QString::number(it->second->idPrueba);
        qDebug() << comando;

        if (QDir().mkpath(comando)){
            qDebug() << "Funciona";
        }

        qDebug() << "Despues de mkdir";
        //int codeError= runCaptureExectuable(comando, it->second->video->idVideo, it->second->video->nombreVideo);
        qDebug()<<"Despues de run";
        /*switch(codeError){
        case 0:
            //No error
            break;

        case -1:
            // Error con los parámetros
            break;
        case -2:
            //No se pudo reproducir el video
            break;

        case 1:
            //Fallo con el EyeTracker
            break;
        case 2:
            //Fallo con la webCam
            break;
        case 3:
            //Fallo con ambos
            break;
        default:
            //Fallo de la función runCaptureExectuable->CreateProcess al intentar llamar prueba.exe
            break;
        }*/
        QString cmd_system = "prueba.exe "+ comando + "/prueba.txt " + comando + "/pupila.txt Data/Videos/"+QString::number(it->second->video->idVideo)+"/"+it->second->video->nombreVideo;
        qDebug() << cmd_system;
        system(cmd_system.toUtf8().constData());

        /**********************************************Archivamos traza**********************************************/
        QDir().mkpath("Trazas/"+it->second->video->nombreVideo);
        QDir dir("Trazas/"+it->second->video->nombreVideo);
        dir.setFilter(QDir::AllEntries | QDir::NoDotAndDotDot);
        int index = dir.count();
        QFile::copy(comando  + "/prueba.txt ","Trazas/"+it->second->video->nombreVideo + "/" + "traza"+ QString::number(index));
        /***********************************************************************************************************/

        //Llamada a calcular parametros
        getParameters(it->second, session);
        qDebug()<<"Vamos a actualizar";
        jsonDownloader->actualizeJSON(it->second);

    }

    //Tras crear actualiza la lista de sesiones en el lateral izquierdo
    //Crea un botón por cada sesion
    clearLayout(ui->LY_Sesiones);
    for (map<int,Sesion*>::iterator it=currentPacient->sesiones.begin(); it != currentPacient->sesiones.end(); ++it){
        qDebug() << "Sesion: " + QString::number(it->first);
        QString sesionName = "Sesion_" + QString::number(it->first);
        QPushButton *BSesion = new QPushButton(sesionName);
        connect(BSesion, SIGNAL(clicked()), this, SLOT(ShowPruebas()));
        ui->LY_Sesiones->addWidget(BSesion);
    }

     //Regresa a vista de paciente al terminar la sesion?
     if(ui->stackedWidget->currentIndex() != 3) ui->stackedWidget->setCurrentIndex(3);
}

void MainWindow::SubmitPaciente(void)
{
    if( ui->stackedWidget->currentIndex() != 2) ui->stackedWidget->setCurrentIndex(2);

    //new Paciente;
    Paciente *newP = new Paciente();
    newP->mName = ui->IntroNombre->text();
    newP->mApel = ui->IntroApellidos->text();
    newP->mData = ui->IntroNacimiento->dateTime();
    newP->mDNI  = ui->IntroDNI->text();
    newP->idPaciente = contPacienteID;

    jsonDownloader->sendJSON(newP);
}


//Muestra por pantalla un QImage
void MainWindow::updatePlayerUI(QImage img)
{
    if(!img.isNull()){
        ui->LScreen->setAlignment(Qt::AlignCenter);
        ui->LScreen->setPixmap(QPixmap::fromImage(img).scaled(ui->LScreen->size(), Qt::KeepAspectRatio, Qt::FastTransformation));
    }
}

void MainWindow::PlayVideo()
{
        if (ui->CBzonas->isChecked()){
            myPlayer->mode = 3;
        }

        if(ui->CBPupila->isChecked())
            myPlayer->mostrarPupila = true;
        else
            myPlayer->mostrarPupila = false;

        if((myPlayer->mode == 1 || myPlayer->mode == 3) && ui->CBSimpleGaze->isChecked())
            if(ui->CBzonas->isChecked())
                myPlayer->mode = 8;
            else
                myPlayer->mode = 7;

        if(ui->CBwebcam->isChecked())
            myPlayer->mostrarCam = true;

        qDebug() << "Mode is " << myPlayer->mode;
        if(myPlayer->isStopped()){
            qDebug() << "My player is stopped, play";
            myPlayer->Play();
            ui->BPlay->setText(tr("Stop"));
        }
        else{
            myPlayer->Stop();
            ui->BPlay->setText(tr("Play"));
        }
}

//Abre una nueva ventana en la que mostrar la traza obtenida
void MainWindow::VideoTraza(){
        qDebug() << "LLEGA";
        int mode=1;
        currentPrueba = currentSesion->pruebas.at(splitID(ui->TB_Pruebas->tabText(ui->TB_Pruebas->currentIndex())));
        qDebug() << "LLEGA";
        currentVideo = currentPrueba->video;
        qDebug() << "LLEGA";
        qDebug() << "Estoy en la pestaña " << ui->TB_Pruebas->currentIndex() << "Correspondiente a la prueba con prueba.txt";
        if(ui->stackedWidget->currentIndex() != 4) ui->stackedWidget->setCurrentIndex(4);
        if(ui->stackedWidget_2->currentIndex() != 0) ui->stackedWidget_2->setCurrentIndex(0);



        createPlayerThread(mode);

}

int MainWindow::splitID(QString s)
{
    //Los nombres tienen van a ser tipo: Paciente_1
    return  s.split( "_" )[1].toInt();
}

void MainWindow::clearLayout(QLayout *layout){
    QLayoutItem *item;
    while((item = layout->takeAt(0))) {
        if (item->layout()) {
            clearLayout(item->layout());
            delete item->layout();
        }
        else if (item->widget()) {
            delete item->widget();
        }
    }
}


void MainWindow::SelectROILabel(){

    ROILabel myROILabel;
    myROILabel.setModal(true);
    myROILabel.setLabelList(etiquetas);
    connect(&myROILabel, SIGNAL(event_or_not(bool)), this, SLOT(setEvent(bool)));
    connect(&myROILabel, SIGNAL(setLabelName(QString)), this, SLOT(setLabel(QString)));
    myROILabel.exec();

    ui->stackedWidget_2->setCurrentIndex(3);
    ui->stackedWidget->setCurrentIndex(4);
    qDebug() << "Mode is " << mode;
    createPlayerThread(mode);
}

void MainWindow::setLabel(QString lb){//int id_label){
    //label = etiquetas.at(id_label)->nombreEtiqueta;
    label = lb;
}

void MainWindow::resetPlayer(){
    qDebug() << "Reseteando player";
       //Limpio la pantalla y el botón de play
       ui->BPlay->setText(tr("Play"));
       ui->LScreen->setPixmap(QPixmap(ui->LScreen->size()));

       //Desmarco todos los checkbox
       ui->CBPupila->setChecked(false);
       ui->CBSimpleGaze->setChecked(false);
       ui->CBwebcam->setChecked(false);
       ui->CBzonas->setChecked(false);
       ui->CBzona_marcadas->setChecked(false);
       ui->CB_zonas_identificadas->setChecked(false);

       //Muestro lista de vídeso y cambio de vista
       ShowVideos();
       if( ui->stackedWidget->currentIndex() != 0) ui->stackedWidget->setCurrentIndex(0);
}


void MainWindow::getParameters(Prueba *prueba, Sesion *sesion){
    qDebug() << "GetParams";

    Point previousPoint, currentPoint;
    QList<Fixation> fixationsList;
    map<int, Point> gazePlotData;
    int fixationThresh_ms, fixationThresh_frames, auxFixations, frames_00;
    float delay, distSamePoint;

    /***Inicializar***/
    auxFixations = frames_00 = 0;

    fixationThresh_ms = ui->SFixTime->value();
    distSamePoint = ui->SFixRad->value();
    qDebug() << "ficheroTraza";
    QString ficheroTraza = "Data/Pacientes/" + QString::number(currentPacient->idPaciente) + "/" +
                            QString::number(sesion->idSesion) + "/" + QString::number(prueba->idPrueba)
                            + "/prueba.txt";
    qDebug() << "ficheroTraza";
    gazePlotData = readData(ficheroTraza.toUtf8().constData());
    qDebug() << "Voy a obtener los parametros de: " << ficheroTraza;
    delay = (1000/prueba->video->fps);
    fixationThresh_frames = fixationThresh_ms / delay;
    qDebug() << "THresh frames: " << fixationThresh_frames;
    qDebug() << "Antes del for";
    map<int,Point>::iterator it = gazePlotData.begin();
    previousPoint = currentPoint = it->second;
    it++;
    for (it; it != gazePlotData.end(); it++) {
        if(it->second.x == 0 || it->second.y == 0) continue;
        if (dist(currentPoint, it->second) < distSamePoint) {
            if (auxFixations >= fixationThresh_frames){
                if (fixationsList.size() == 0) {
                    prueba->t_hasta1aFijacion = it->first*delay + fixationThresh_ms;
                    qDebug() << "Tiempo a primera fijación: " << prueba->t_hasta1aFijacion << "(ms)";
                    Fixation auxFix;
                    auxFix.coord = currentPoint;
                    auxFix.frame = it->first;
                    auxFix.duration = fixationThresh_frames;
                    fixationsList.push_back(auxFix);
                }
                else if(dist(currentPoint, fixationsList.at(fixationsList.length()-1).coord) < distSamePoint ){
//                    qDebug() << "Peta por esto";
                    fixationsList[fixationsList.length()-1].duration++;
                }
                else{
//                    qDebug() << "O por esto?";
                    Fixation auxFix;
                    auxFix.coord = currentPoint;
                    auxFix.frame = it->first;
                    auxFix.duration = fixationThresh_frames;
                    fixationsList.push_back(auxFix);
                }
            }
            auxFixations++;
        }
        else {
//            qDebug() << "Aqui no creo";
            previousPoint = currentPoint;
            currentPoint = it->second;
            auxFixations = 0;
        }
    }

    //Calculate video parameters
    qDebug() << "Tiempo total video: " << gazePlotData.size() * delay << "(ms)";

    prueba->fixationsList = fixationsList;
    prueba->num_Fijaciones = fixationsList.size();
    qDebug() << "Numero de fijaciones: " <<  fixationsList.size();
    qDebug() << "Fixations: ";
    for(auto it = fixationsList.begin(); it != fixationsList.end();it++){
        qDebug() << "> At (" << it->coord.x << ", " << it->coord.y << ") " << "in " << it->frame << " for " << it->duration << "frames";
//        prueba->t_TotalFijaciones += fixationThresh_ms;
        prueba->t_TotalFijaciones += it->duration * delay;
        prueba->t_MedioFijaciones += it->duration * delay / fixationsList.size();
    }
    qDebug() << "Tiempo total fijaciones: " << prueba->t_TotalFijaciones << "(ms)";
    qDebug() << "Tiempo medio fijaciones: " << prueba->t_MedioFijaciones << "(ms)";
    prueba->pct_Fijaciones = prueba->t_TotalFijaciones / gazePlotData.size() / delay;
    qDebug() << "Porcentaje de fijac    iones: " << prueba->pct_Fijaciones * 100 << "(%)";
    prueba->t_1aFijacion = fixationsList.begin()->duration * delay;
    qDebug() << "Tiempo hasta la primera fijacion: " << prueba->t_1aFijacion << "(ms)";

    //Calculate zones parameters
//    map<int, float> pct_FijacionXZona, t_FijacionXZona;
//    map<int,int> num_fijacionesXZona;
    qDebug() << "A cacluclar zonas";
    if(prueba->video->zonas.empty()){
    qDebug() <<"No habia zonas";
        return;
    }
    for(auto f = fixationsList.begin(); f != fixationsList.end(); f++){
        qDebug() << "Entro al for  checkeo la fix (" << f->coord.x << ", " << f->coord.y << ")";
        qDebug() << prueba->video->zonas.count(f->frame);
        for(auto z = prueba->video->zonas.begin(); z != prueba->video->zonas.end();z++){
            if(z->second->isFixIn(*f) && f->frame >= z->second->frameStart){
                   qDebug() << "F frame: " << f->frame;
                   qDebug() << "Comienzo evento: " << z->second->frameStart;
                   if(!prueba->zone_fixationsList.count(z->first)){
                         QList<Fixation> auxFixList;
                         auxFixList.append(*f);
                         prueba->zone_fixationsList[z->first] = auxFixList;
                         prueba->t_hasta1aFijacionXZona[z->first] = (f->frame - z->second->frameStart + fixationThresh_frames) * delay; //Sin completar
                         prueba->t_1aFijacionXZona[z->first] = f->duration * delay;
                    }
                    else prueba->zone_fixationsList[z->first].append(*f);
                    qDebug() <<"Fijacion en zona";
                    int idZona = z->first;
                    qDebug() << "Lei id zona";
                    prueba->num_fijacionesXZona[idZona]++;
                    qDebug() << "Incremente fixNum";
                    prueba->t_FijacionXZona[idZona] += f->duration * delay;
                    qDebug() << "Meti tiempo";
                    prueba->pct_FijacionXZona[idZona] += f->duration / gazePlotData.size();
                    qDebug() << "Hice el pct";
           }
       }
    }

}

void MainWindow::readPatients(QList<Paciente *> patients){
    foreach(Paciente* retrievedPatient, patients){
        pacientes[retrievedPatient->idPaciente] = retrievedPatient;
    }
}
void MainWindow::readPatientInfo(Paciente* patient, QList<Sesion*> sessions){

    //Comprobamos si el paciente existe
    map<int,Paciente*>::iterator it = pacientes.find(patient->idPaciente);
    if(it != pacientes.end()){
        foreach(Sesion* retrievedSession, sessions)
            pacientes[patient->idPaciente]->sesiones[retrievedSession->idSesion] = retrievedSession;

       displaySessions(it->second);
    }
}
void MainWindow::readNewPatient(Paciente *patient){
    pacientes[patient->idPaciente] = patient;
    ShowPacientes();
}

void MainWindow::readSessions(QList<Sesion*> lista){
    foreach(Sesion* retrievedSession, lista){
        //ui->textBrowser->append(QString::number(retrievedSession->idSesion));
        map<int,Paciente*>::iterator it = pacientes.find(retrievedSession->idPaciente);
        if(it != pacientes.end()){
            pacientes[retrievedSession->idPaciente]->sesiones[retrievedSession->idSesion] = retrievedSession;
        }
    }
}

void MainWindow::readSessionInfo(/*Sesion* session,*/ QList<Prueba*> tests){

    //Buscamos
    qDebug(QString::number(tests.first()->idSesion).toUtf8());

    foreach(Prueba* retrievedTest, tests){
        currentSesion->pruebas[retrievedTest->idPrueba] = retrievedTest;
        currentSesion->pruebas[retrievedTest->idPrueba]->video = videos[retrievedTest->idVideo];
        qDebug("Test añadido xD ");
    }
    displayTests(currentSesion->pruebas);


    /*
    qDebug(QString::number(session->idPaciente).toUtf8());
    //Buscamos al paciente
    std::map<int,Paciente*>::iterator it = pacientes.find(session->idPaciente);
    if(it != pacientes.end()){

        qDebug("Si que conocemos a este paciente :) ");
        //Si hay pruebas
        if(!tests.empty()){
            qDebug("Y la lista de tests que me diste no estaba vacía ;) ");
            //Buscamos el puntero a la sesión correcta
            std::map<int, Sesion*>::iterator iter = it->second->sesiones.find(session->idSesion);
            if(iter != it->second->sesiones.end()){

                qDebug("También he encontrado la sesión (Fuck, yeah!) ");
                //Cuando se encuentra, se añaden todas las pruebas


                //Y rellenamos la vista
            }
        }
    }*/
}

void MainWindow::readVideos(QList<Video*> lista){
    foreach(Video* retrievedVideo, lista){
        videos[retrievedVideo->idVideo] = retrievedVideo;
    }
}

void MainWindow::readVideoInfo(Video* video, QList<Zona*> zonas){

    map<int,Video*>::iterator it = videos.find(video->idVideo);
    if(it != videos.end()){
        foreach(Zona* retrievedZone, zonas){
            it->second->zonas[retrievedZone->idZona] = retrievedZone;
        }
    }
}

void MainWindow::readNewVideo(Video* video){
    videos[video->idVideo] = video;

    QString comando = "Data/Videos/"+QString::number(video->idVideo);
    QDir().mkdir(comando);
    QFile::copy(video->originalFileName, comando+"/"+video->nombreVideo);

    ShowVideos();
}

void MainWindow::readNewZone(Zona* zone){
    map<int, Video*>::iterator it = videos.find(zone->idVideo);
    if(it!=videos.end()){
        it->second->zonas[zone->idZona] = zone;
    }

}

void MainWindow::readLabels(QList<Etiqueta*> labels){
    etiquetas = labels;
}
void MainWindow::readNewLabel(Etiqueta* label){
    etiquetas.append(label);
}


map<int, cv::Point> MainWindow::readData(String filename) {
    ifstream f;
    f.open(filename,ios::in);
    map<int, Point> auxMap;
    if(f.is_open()){
        while (!f.eof()) {
            int x, y, frame;
            f >> frame >> x >> y;
            auxMap.insert(pair<int, Point>(frame, Point(x, y)));
        }
        f.close();
        return auxMap;
    }
    else{
        qDebug() << "No se ha podido leer el fichero de traza.";
        auxMap.clear();
        return auxMap;
    }
}

void MainWindow::StartEndEvent(){
    if(myPlayer->mode == 9)
            myPlayer->mode = 4;
    else if(myPlayer->mode == 5)
            myPlayer->mode = 6;
    myPlayer->Stop();
    ui->BPlay->setText(tr("Play"));
    myPlayer->reproduce();
}

void MainWindow::setEvent(bool isEvent){
    qDebug() << "Is event? " << isEvent;
    if(isEvent){
        ui->BEvent->setEnabled(true);
        mode = 9;
    }
    else{
        ui->BEvent->setEnabled(false);
        mode = 2;
    }
}

float MainWindow::dist(Point a, Point b) {
    Point2f i((float)a.x, (float)a.y);
    Point2f j((float)b.x, (float)b.y);

    float x = i.x - j.x;
    float y = i.y - j.y;

    return x*x + y*y;
}

void MainWindow::displaySessions(Paciente* patient){

    clearLayout(ui->LY_Sesiones);
    if (patient->sesiones.empty()){
        ui->LnameHisto->setText("No hay ninguna sesión seleccionada.");
        ui->LdateHisto->setText("");
        ui->LfixPctHisto->setText("");
    }
    else{
        ui->LnameHisto->setText(" ");
        ui->LdateHisto->setText("Fecha: ");
        ui->LfixPctHisto->setText(" ");
    }

     //Crea un botón por cada sesion
    for (map<int,Sesion*>::iterator iter=patient->sesiones.begin(); iter != patient->sesiones.end(); ++iter){
        qDebug() << "Sesion: " + QString::number(iter->first);
        QString sesionName = "Sesion_" + QString::number(iter->first);
        QPushButton *BSesion = new QPushButton(sesionName);
        connect(BSesion, SIGNAL(clicked()), this, SLOT(ShowPruebas()));
        ui->LY_Sesiones->addWidget(BSesion);
    }
}

void MainWindow::displayTests(map<int, Prueba*> tests){

    for(map<int,Prueba*>::iterator it = tests.begin(); it != tests.end(); ++it){
        QString tabName = "Prueba_" + QString::number(it->first);
        QVBoxLayout *vbox = new QVBoxLayout;
        QLabel *num_Fijaciones,*Titulo1,*t_TotalFijaciones,*t_MedioFijaciones,*pct_Fijaciones,*t_hasta1aFijacion,*t_1aFijacion;
        QLabel *Titulo2, *fixzona;

        Titulo2 = new QLabel;
        Titulo1 = new QLabel;
        fixzona = new QLabel;
        num_Fijaciones = new QLabel;
        t_TotalFijaciones = new QLabel;
        t_MedioFijaciones = new QLabel;
        pct_Fijaciones = new QLabel;
        t_hasta1aFijacion = new QLabel;
        t_1aFijacion = new QLabel;

        QPushButton *BTraza = new QPushButton("Ver Traza");
        connect(BTraza,SIGNAL(clicked()),this,SLOT(VideoTraza()));

        QPushButton *BFullGaze = new QPushButton("Full Gaze");
        connect(BFullGaze,SIGNAL(clicked()),this,SLOT(FullGaze()));

        Titulo1->setText("Parámetros generales de vídeo:");
        num_Fijaciones->setText("   Num. fijaciones: " + QString::number(it->second->num_Fijaciones));
        t_TotalFijaciones->setText("    Tiempo total fijaciones: " + QString::number(it->second->t_TotalFijaciones) + " [ms]");
        t_MedioFijaciones->setText("    Tiempo medio fijaciones: " + QString::number(it->second->t_MedioFijaciones) + " [ms]");
        pct_Fijaciones->setText("   Porcentaje de fijaciones: " + QString::number(it->second->pct_Fijaciones*100) +"%");
        t_hasta1aFijacion->setText("    Tiempo hasta la 1ª fijacion: " + QString::number(it->second->t_hasta1aFijacion) + " [ms]");
        t_1aFijacion->setText("    Tiempo de la 1ª fijación: " + QString::number(it->second->t_1aFijacion) + " [ms]");
        Titulo1->setText("Parámetros de zonas:");
        fixzona->setText("Número de fijaciones por zona");

        vbox->addWidget(Titulo1);
        vbox->addWidget(num_Fijaciones);
        vbox->addWidget(t_TotalFijaciones);
        vbox->addWidget(t_MedioFijaciones);
        vbox->addWidget(pct_Fijaciones);
        vbox->addWidget(t_hasta1aFijacion);
        vbox->addWidget(t_1aFijacion);
        vbox->addWidget(Titulo2);
        vbox->addWidget(fixzona);

        qDebug() << "Antes for";

        qDebug() << "Shoe parametros por zona";
        for(auto al = it->second->num_fijacionesXZona.begin(); al != it->second->num_fijacionesXZona.end(); al++ ){
            qDebug() << "Num fix";
            QString title = "   Numero de fijaciones por zona";
            QString linea_zona = "      Zona_" + QString::number(al->first) + ": " +  QString::number(al->second) + " fijaciones.";
            QLabel *auxTitle = new QLabel;
            QLabel *auxLabel = new QLabel;
            auxTitle->setText(title);
            auxLabel->setText(linea_zona);
            vbox->addWidget(auxTitle);
            vbox->addWidget(auxLabel);
        }
        qDebug() << "Donde muere!";
        qDebug() << "Id paciente";
        QString ruta = "Data/Pacientes/" + QString::number(currentPacient->idPaciente);
        qDebug() << "Id sesion";
        ruta += "/" + QString::number(currentSesion->idSesion);
        qDebug() << "Id prueba";
        ruta += "/" + QString::number(it->first);
        qDebug() << "Aqui!";

        QString donut_cmd = "donutchart.py " + ruta + " ";
        for(auto al = it->second->pct_FijacionXZona.begin(); al != it->second->pct_FijacionXZona.end(); al++){
            try{
            qDebug() << "Donut chart";
            //Llamar aqui a donutchart.py
            QString title = "   Porcentaje de fijaciones por zona";
            qDebug() <<"Donut chart 2";
            QString linea_zona = "      Zona_" + QString::number(al->first) + ": " +  QString::number(al->second * 100) + "%";
            qDebug()<<"Donut chart 3";
            donut_cmd = donut_cmd + currentPrueba->video->zonas.at(al->first)->label + " " + al->second;
            qDebug() << "Comando donut: " + donut_cmd;
            QLabel *auxTitle = new QLabel;
            QLabel *auxLabel = new QLabel;
            auxTitle->setText(title);
            auxLabel->setText(linea_zona);
            vbox->addWidget(auxTitle);
            vbox->addWidget(auxLabel);}
            catch(...){

            }
        }
//        for(auto al = it->second->t_FijacionXZona.begin(); al != it->second->t_FijacionXZona.end(); al++){
//            QString ax = "      Zona_" + QString::number(al->first) + ": " +  QString::number(al->second) + " [ms]";
//            QLabel *aux = new QLabel;
//            aux->setText(linea_zona);
//            vbox->addWidget(aux);
//        }
        for(auto al = it->second->t_hasta1aFijacionXZona.begin(); al != it->second->t_hasta1aFijacionXZona.end(); al++){
            qDebug() << "T 1a fix";
            QString title = "   Tiempo hasta la 1ª fijación por zona";
            QString linea_zona = "      Zona_" + QString::number(al->first) + ": " +  QString::number(al->second) + " [ms]";
            QLabel *auxTitle = new QLabel;
            QLabel *auxLabel = new QLabel;
            auxTitle->setText(title);
            auxLabel->setText(linea_zona);
            vbox->addWidget(auxTitle);
            vbox->addWidget(auxLabel);
        }
        for(auto al = it->second->t_1aFijacionXZona.begin(); al != it->second->t_1aFijacionXZona.end(); al++){
            qDebug() << "Duracion 1a";

            QString title = "   Duracion de la 1ª fijación por zona";
            QString linea_zona = "      Zona_" + QString::number(al->first) + ": " +  QString::number(al->second) + " [ms]";
            QLabel *auxTitle = new QLabel;
            QLabel *auxLabel = new QLabel;
            auxTitle->setText(title);
            auxLabel->setText(linea_zona);
            vbox->addWidget(auxTitle);
            vbox->addWidget(auxLabel);
        }

        vbox->addWidget(BTraza);
        vbox->addWidget(BFullGaze);

        //Obtenemos los datos para la pupila
                                                                                                                                        //Esto estaba it->secon->idPrueba
        QFile file("Data/Pacientes/" + QString::number(currentPacient->idPaciente) + "/" + QString::number(it->second->idSesion) + "/" + QString::number(it->first) + "/pupila.txt");
           if(file.open(QIODevice::ReadOnly)) {

               QVector<double>* vectorX = new QVector<double>();
               QVector<double>* vectorY = new QVector<double>();
               QTextStream in(&file);

               std::map<double, double> zeroPupil;

               while(!in.atEnd()) {
                   QString line = in.readLine();
                   QStringList fields = line.split(" ");

                   if(fields.at(1).toDouble()==0){
                       zeroPupil[fields.at(0).toDouble()] = fields.at(1).toDouble();
                   }
                   else{
                       if(zeroPupil.size()>=4){
                           std::map<double, double>::iterator iterador = zeroPupil.begin();
                               while(iterador != zeroPupil.end())
                               {
                                   vectorX->append(iterador->first);
                                   vectorY->append(iterador->second);
                                   iterador++;
                               }
                       }
                       zeroPupil.clear();
                       vectorX->append(fields.at(0).toDouble());
                       vectorY->append(fields.at(1).toDouble());
                   }
               }

               file.close();

            //Gráfica pupila
            if(vectorX->count()!=0){
                QCustomPlot *pupilPlot = new QCustomPlot();//new QWidget();
                vbox->addWidget(pupilPlot);

                pupilPlot->setMinimumSize(450, 250);
                //pupilPlot->resize(450, 250);
                pupilPlot->addGraph(0);
                pupilPlot->graph(0)->setScatterStyle(QCPScatterStyle::ssNone);
                pupilPlot->graph(0)->setLineStyle(QCPGraph::lsLine);

                pupilPlot->xAxis->setLabel("Tiempo (s)");
                pupilPlot->yAxis->setLabel("Tamaño de pupila (normalizada)");

                pupilPlot->xAxis->setRange(0, vectorX->last());
                pupilPlot->yAxis->setRange(0, 1);

                pupilPlot->graph(0)->setData(*vectorX, *vectorY);
                pupilPlot->replot();
                pupilPlot->update();
            }
        }

         qDebug() << "Aqui!";

       //Llamamos al script de python de calculo de desviacines
           QString dev_cmd = "python DeviationAnalysis.py " + QString::number(it->second->video->idVideo);
           qDebug() << "Aqui!";
           dev_cmd += " " +  QString::number(it->second->num_Fijaciones);
           dev_cmd += " " +  QString::number(it->second->pct_Fijaciones);
           dev_cmd += " " +  QString::number(it->second->t_MedioFijaciones);
           dev_cmd += " " +  QString::number(it->second->t_hasta1aFijacion);
           dev_cmd += " " +  QString::number(it->second->t_1aFijacion);

           dev_cmd += " " +  ruta;
           qDebug() << "COmado deviation: " << dev_cmd;
           system(dev_cmd.toUtf8().constData());
           QLabel *LB_Deviation_numfix, *LB_Deviation_pctfix, *LB_Deviation_t1fix, *LB_Deviation_thasta1fix, *LB_Deviation_tmediofix, *LB_DeviationAnalysis;

           LB_Deviation_numfix = new QLabel();
           LB_Deviation_pctfix = new QLabel();
           LB_Deviation_t1fix = new QLabel();
           LB_Deviation_thasta1fix = new QLabel();
           LB_Deviation_tmediofix = new QLabel();
           LB_DeviationAnalysis = new QLabel();

           //        QImage Deviation_numfix_im(ruta + "/Deviation_numfix.png");
           //        vbox->addWidget(&QLabel("Hola"));
           LB_Deviation_numfix->setPixmap(QPixmap::fromImage(QImage(ruta + "/Deviation_numfix.png")));
           LB_Deviation_pctfix->setPixmap(QPixmap::fromImage(QImage(ruta + "/Deviation_pctfix.png")));
           LB_Deviation_t1fix->setPixmap(QPixmap::fromImage(QImage(ruta + "/Deviation_t1fix.png")));
           LB_Deviation_thasta1fix->setPixmap(QPixmap::fromImage(QImage(ruta + "/Deviation_thasta1fix.png")));
           LB_Deviation_tmediofix->setPixmap(QPixmap::fromImage(QImage(ruta + "/Deviation_tmediofix.png")));
           LB_DeviationAnalysis->setPixmap(QPixmap::fromImage(QImage(ruta + "/DeviationAnalysis.png")));

           vbox->addWidget(LB_Deviation_numfix);
           vbox->addWidget(LB_Deviation_pctfix);
           vbox->addWidget(LB_Deviation_t1fix);
           vbox->addWidget(LB_Deviation_thasta1fix);
           vbox->addWidget(LB_Deviation_tmediofix);
           vbox->addWidget(LB_DeviationAnalysis);

           QScrollArea *scrollPruebas = new QScrollArea();
           QWidget *scrollContent = new QWidget();
           scrollContent->setSizePolicy(QSizePolicy::MinimumExpanding,QSizePolicy::Preferred);
           scrollPruebas->setWidgetResizable(true);
           scrollContent -> setLayout(vbox);
           scrollPruebas->setWidget(scrollContent);

           ui->TB_Pruebas->addTab(scrollPruebas, tr(tabName.toUtf8().constData()));
           ui->TB_Pruebas->setCurrentIndex(1);
    }
}

int MainWindow::runCaptureExectuable(QString pathToFolder, int idVideo, QString nameVideo){
//"prueba.exe "+ comando + "/prueba.txt " + comando + "/pupila.txt Data/Videos/"+QString::number(it->second->video->idVideo)+"/"+it->second->video->nombreVideo;
        STARTUPINFO StartupInfo;
        PROCESS_INFORMATION ProcessInfo;


        qDebug("0");

        ZeroMemory(&StartupInfo, sizeof(StartupInfo));
        StartupInfo.cb = sizeof(StartupInfo);
        ZeroMemory(&ProcessInfo, sizeof(ProcessInfo));

        qDebug("1");
        //Preparamos las variables para llamar
        char params[500];
        QString paramsString = "prueba.exe "+ pathToFolder + "/prueba.txt " + pathToFolder + "/pupila.txt Data/Videos/"+QString::number(idVideo)+"/"+nameVideo;
        qDebug("23456");

        std::copy(paramsString.toStdString().begin(), paramsString.toStdString().end(), params);

        //params = paramsString.toStdString().c_str();

        qDebug("4567");
        wchar_t* WParams = new wchar_t[50];
        size_t outSizeParams;

        mbstowcs_s(&outSizeParams, WParams, strlen(params) + 1, params, strlen(params));
        LPWSTR paramsExecutable = WParams;

        qDebug("1");
        // Llamamos y esperamos a que acabe
        if(CreateProcess(NULL, paramsExecutable, NULL, NULL, TRUE, 0, NULL, NULL, &StartupInfo, &ProcessInfo )==false) return 400;

        qDebug("2");
        WaitForSingleObject(ProcessInfo.hProcess,INFINITE);

        // Capturamos el valor devuelto
        DWORD exitCode;
        int codeReturned;

        qDebug("3");

        if( GetExitCodeProcess(ProcessInfo.hProcess, &exitCode) )
            codeReturned = exitCode;
        else
            codeReturned = 400;

        qDebug("4");

        //CloseHandle(ProcessInfo.hThread);
        //CloseHandle(ProcessInfo.hProcess);

        return codeReturned;
}

MainWindow::~MainWindow()
{
    delete myPlayer;
    delete ui;
    delete currentPacient;
//    delete currentSesion;
   // delete currentVideo;
}





void MainWindow::on_SFixTime_valueChanged(int value)
{
    ui->LFixTime->setText("Fix time: " + QString::number(value));
}

void MainWindow::on_SFixRad_valueChanged(int value)
{
    ui->LFixRad->setText("Dist. same point: " + QString::number(value));
}
