#ifndef PLAYER_H
#define PLAYER_H

#include <QMutex>
#include <QScreen>
#include <QThread>
#include <QImage>
#include <time.h>
#include <windows.h>
#include <QWaitCondition>
#include <QDebug>
#include <fstream>
#include "wtypes.h"

#include <opencv2/core/core.hpp>
#include <opencv/cv.h>
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/video/video.hpp"
#include "opencv2/calib3d.hpp"
#include "opencv2/tracking.hpp"

#include <video.h>
#include "fixation.h"

#include "prueba.h"

#include <iomanip>
#include <sstream>

using namespace cv;
using namespace std;

class Player : public QThread
{    Q_OBJECT

 public slots:
     void reproduce();

 signals:
 //Signal to output frame to be displayed
    void processedImage(const QImage &image);
    void go_video();
    void finished();
    void error(QString err);
//     void run();

 public:
    /***Variables globales***/      
    map<int,QList<Point>> auxZones;
    map<int, Video*> videos;
    int videoID;
     QMutex mutex;
     QWaitCondition condition;
     bool stop, mostrarCam, errorCam, first, mostrarPupila;
     int frameRate, frameCount, auxFixations, mode;
     int fixationThresh_ms, fixationThresh_frames, distSamePoint;
     float time_ms, pointScaleFactor = 1;
     Point currentPoint, previousPoint;
     QList<Point> previousPoints, fixationList;
     QList<Fixation> fixList;
     Mat frame, RGBframe;
     map<int, Point> gazePlotData;
     VideoCapture capture, cam;
     QImage img;
     Ptr<Tracker> tracker;
     Rect2d bbox;
     map<pair<int,int>,float> fixationsTime;

     map<int,float> pupilData;
     float timeFirstFixation;
     int delay;

     QString label;

     Prueba* currentPrueba;
     Video *currentVideo;

     void setCurrentPrueba(Prueba* prueba);


    /***Cabeceras***/
    Player(QObject *parent = 0);
    ~Player();
    void setMode(int i);
    void Play();
    void Stop();
    bool loadVideo(Video* myVideo);
    bool isStopped() const;
    void setGazePlotFile(QString gazeName);

    void GazePlot(Mat img, int frameCount);
    float dist(Point a, Point b);
    map<int, cv::Point> readData(String filename);
    void imshow_u(Mat frame);
    void getFixList(QList<Fixation> fix);
    map<int,float> readPupil(String filename);
    void SimpleGazePlot(Mat img, int frameCount);
};

#endif // PLAYER_H

