#include "fixation.h"

Fixation::Fixation()
{
    frame = duration = 0;
    coord = cv::Point(0,0);
}

Fixation::Fixation(int x, int y){
    frame = duration = 0;
    coord = cv::Point(x,y);
}
Fixation::Fixation(QJsonObject JsonObject){

    if(JsonObject["frame"]!=NULL && JsonObject["frame"].toString()!="null")
        frame = JsonObject["frame"].toString().toInt();
    else
        frame = -1;

    if(JsonObject["duration"]!=NULL && JsonObject["duration"].toString()!="null")
        duration = JsonObject["duration"].toString().toInt();
    else
        duration= -1;

    if(JsonObject["x"]!=NULL && JsonObject["y"]!=NULL && JsonObject["x"].toString()!="null"&& JsonObject["y"].toString()!="null")
        coord = cv::Point(JsonObject["x"].toString().toInt(), JsonObject["y"].toString().toInt());
}
