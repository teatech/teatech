#include "jsondownloader.h"
#include <QDebug>

JSONdownloader::JSONdownloader(QObject *parent) : QObject(parent)
{
    connect(manager, SIGNAL(finished(QNetworkReply*)),this, SLOT(JSONReceived(QNetworkReply*)) );
}

// POST
void JSONdownloader::sendJSON(Paciente* patient){

    tempPatient = patient;
    QString jsonStringString = "{\"date\": \""+patient->mData.toString("yyyy-MM-dd")+"\", \"dni\": \""+patient->mDNI+"\", \"id\": \""+QString::number(patient->idPaciente)+"\", \"name\": \""+patient->mName+"\", \"surname\": \""+patient->mApel+"\", \"surname2\": \"\"}";
    QByteArray jsonString = jsonStringString.toUtf8();
    sendJSON("patients", jsonString, "patient");
}
void JSONdownloader::sendJSON(Sesion* session, QList<Prueba*> tests){

    tempSession= session;
    tempTests = tests;
    sendingNewInfo = true;
    QString jsonStringString = "{\"id\": \""+QString::number(session->idSesion)+"\", \"idPatient\": \""+QString::number(session->idPaciente)+"\", \"date\": \""+session->fechaSesion.toString("yyyy-MM-dd")+"\"}";
    QByteArray jsonString = jsonStringString.toUtf8();
    sendJSON("sessions", jsonString, "session");
}
void JSONdownloader::sendJSON(Prueba* test){

    QString jsonStringString = "{\"id\": \""+QString::number(test->idPrueba)+"\", \"idVideo\": \""+QString::number(test->idVideo)+"\", \"idSession\": \""+QString::number(test->idSesion)+"\"}";
    QByteArray jsonString = jsonStringString.toUtf8();
    sendJSON("tests", jsonString, "test");
}
void JSONdownloader::sendJSON(Video* video){

    tempVideo= video;
    QString jsonStringString = "{\"id\": \""+QString::number(video->idVideo)+"\", \"description\": \""+video->nombreVideo+"\"}";
    QByteArray jsonString = jsonStringString.toUtf8();
    sendJSON("videos", jsonString, "video");
}
void JSONdownloader::sendJSON(Zona* zone){

    tempZone = zone;
    QString jsonStringString = "{\"id\": \""+QString::number(zone->idZona)+"\", \"idVideo\": \""+QString::number(zone->idVideo)+"\", \"idLabel\": \""+QString::number(zone->idEtiqueta)+"\"}";
    QByteArray jsonString = jsonStringString.toUtf8();
    sendJSON("zones", jsonString, "zone");
}
void JSONdownloader::sendJSON(Etiqueta* label){

    tempLabel = label;
    QString jsonStringString = "{\"id\": \""+QString::number(label->idEtiqueta)+"\", \"description\": \""+label->nombreEtiqueta+"\"}";
    QByteArray jsonString = jsonStringString.toUtf8();
    sendJSON("labels", jsonString, "label");
}
void JSONdownloader::actualizeJSON(Prueba * test){

    //1) PARÁMETROS
    //1.1) General
    QString jsonStringString = "[{\"idParameter\": \"0\", \"idTest\": \""+QString::number(test->idPrueba)+"\", \"idZone\": \"-1\", \"numFixations\": \""+ QString::number(test->num_Fijaciones) +"\", \"tTotalFixations\": \""+ QString::number(test->t_TotalFijaciones) +"\", \"tAvgFixations\": \""+ QString::number(test->t_MedioFijaciones) +"\", \"pctFixations\": \""+ QString::number(test->pct_Fijaciones) +"\", \"tTo1stFixation\": \""+ QString::number(test->t_hasta1aFijacion) +"\", \"t1stFixation\": \""+ QString::number(test->t_1aFijacion) +"\"  }]";
    qDebug(jsonStringString.toUtf8());
    sendJSON("parameters", jsonStringString.toUtf8(), "parameter");

    //1.2)Por zonas
    for(map<int, int>::iterator iterZones = test->num_fijacionesXZona.begin(); iterZones!=test->num_fijacionesXZona.end(); ++iterZones){
        float pctFix =0, tFix =0;
        if(test->pct_FijacionXZona.find(iterZones->first)!=test->pct_FijacionXZona.end())
            pctFix= test->pct_FijacionXZona[iterZones->first];
        if(test->t_FijacionXZona.find(iterZones->first)!=test->t_FijacionXZona.end())
            tFix = test->t_FijacionXZona[iterZones->first];


        QString jsonStringString = "{\"idParameter\": \"0\", \"idTest\": \""+QString::number(test->idPrueba)+"\", \"idZone\": \""+ QString::number(iterZones->first) +"\", \"numFixations\": \""+ QString::number(iterZones->second) +"\", \"tTotalFixations\": \""+ QString::number(tFix) +"\", \"tAvgFixations\": \"0\", \"pctFixations\": \""+ QString::number(pctFix) +"\", \"tTo1stFixation\": \"0\", \"t1stFixation\": \"0\"  }";
        qDebug(jsonStringString.toUtf8());
        sendJSON("parameters", jsonStringString.toUtf8(), "parameter");
    }

    //2) FIJACIONES
    for(QList<Fixation>::iterator iterFix = test->fixationsList.begin(); iterFix!=test->fixationsList.end(); ++iterFix){
        QString jsonStringString = "{\"id\": \"0\", \"idTest\": \""+QString::number(test->idPrueba)+"\", \"frame\": \""+ QString::number((*iterFix).frame) +"\", \"duration\": \""+ QString::number((*iterFix).duration) +"\", \"x\": \""+ QString::number((*iterFix).coord.x) +"\", \"y\": \""+ QString::number((*iterFix).coord.y) +"\"  }";
        qDebug(jsonStringString.toUtf8());
        sendJSON("fixations", jsonStringString.toUtf8(), "fixation");
    }

}
void JSONdownloader::sendJSON(QString urlTarget, QByteArray jsonToSend, QString nameObject)
{
    QByteArray postDataSize = QByteArray::number(jsonToSend.size());
    QNetworkRequest request(QUrl(url+urlTarget+"/insert/"+nameObject));
    request.setRawHeader("User-Agent", "TEAtech application");
    request.setRawHeader("X-Custom-User-Agent", "TEAtech application");
    request.setRawHeader("Content-Type", "application/json");
    request.setRawHeader("Content-Length", postDataSize);

    manager->post(request, jsonToSend);
}

// GET

void JSONdownloader::retrieveFromDDBB(QString fromTable){

    QNetworkRequest request(QUrl(url+fromTable+"/search"));

    request.setRawHeader("User-Agent", "TEAtech application");
    request.setRawHeader("X-Custom-User-Agent", "TEAtech application");

    manager->get(request);
}
void JSONdownloader::retrieveFromDDBB(QString fromTable, int idTarget){

    QNetworkRequest request(QUrl(url+fromTable+"/search/"+QString::number(idTarget)));

    request.setRawHeader("User-Agent", "TEAtech application");
    request.setRawHeader("X-Custom-User-Agent", "TEAtech application");

    manager->get(request);
}
void JSONdownloader::retrieveFromDDBB(QString fromTable, QString inRelationWith, int idTarget){

    QNetworkRequest request(QUrl(url+fromTable+"/search/"+inRelationWith+"/"+QString::number(idTarget)));

    request.setRawHeader("User-Agent", "TEAtech application");
    request.setRawHeader("X-Custom-User-Agent", "TEAtech application");

    manager->get(request);
}

void JSONdownloader::JSONReceived(QNetworkReply *reply){

    if(reply->error()== QNetworkReply::NoError){

        QJsonObject jsonReceived;
        QByteArray data = reply->readAll();
        QJsonDocument doc = QJsonDocument::fromJson(data);

        qDebug(data);

        if(!doc.isNull() && doc.isObject()){
            jsonReceived = doc.object();
            if(jsonReceived.isEmpty()) return;

            // RETRIEVE ALL PATIENTS
            if(jsonReceived.contains("allpatients")){

                if(!jsonReceived["allpatients"].toArray().first().toObject().isEmpty()){
                    if(jsonReceived["allpatients"].toArray().first().toObject().contains("name")){

                        QList<Paciente*> listPatients;
                        QJsonArray array = jsonReceived["allpatients"].toArray();
                        for(const QJsonValue& val: array) {
                            Paciente* retrievedPatient = new Paciente(val.toObject());
                            listPatients.push_back(retrievedPatient);
                        }
                        emit(patientsReceived(listPatients));
                    }
                    else{
                        try{
                            int id = jsonReceived["allpatients"].toArray().first().toObject()["id"].toString().toInt();
                            tempPatient->idPaciente = id;
                            qDebug("Nuevo paciente "+QString::number(id).toUtf8());
                            emit(newPatient(tempPatient));
                        }catch(...){  }
                    }
                }
            }

            // RETRIEVE ALL SESSIONS FROM PATIENT X
            else if(jsonReceived.contains("sessionsfrompatient")){

                QJsonObject JsonObject = jsonReceived["sessionsfrompatient"].toArray().first().toObject();
                if(JsonObject.isEmpty()) return;
                Paciente* retrievedPatient = new Paciente(JsonObject);

                QList<Sesion*> sessions;
                QJsonArray arraySessions = JsonObject["sessions"].toArray();
                for(const QJsonValue& valSession: arraySessions) {
                     Sesion* retrievedSession = new Sesion(valSession.toObject());
                     sessions.append(retrievedSession);
                 }
                 emit(sessionsFromPatientReceived(retrievedPatient, sessions));
             }

            // RETRIEVE NEW SESSION
            else if(jsonReceived.contains("sessions")){
                if(!jsonReceived["sessions"].toArray().first().toObject().isEmpty()){
                    if(!jsonReceived["sessions"].toArray().first().toObject().contains("idPatient")){
                        try{
                            int id = jsonReceived["sessions"].toArray().first().toObject()["id"].toString().toInt();
                            tempSession->idSesion = id;

                            itTests = tempTests.begin();
                            if(itTests!=tempTests.end()){
                                (*itTests)->idSesion = id;
                                sendJSON(*itTests);
                            }
                            else
                                emit(newSession(tempSession));

                        }catch(...){  }
                    }

                }
            }

            // RETRIEVE ALL TESTS FROM SESSION X
            else if(jsonReceived.contains("testsfromsession")){

                QJsonArray arrayTests = jsonReceived["testsfromsession"].toArray();
                for(const QJsonValue& valTest: arrayTests){
                    Prueba* retrievedTest = new Prueba(valTest.toObject());
                    tempTests.append(retrievedTest);
                }
                itTests = tempTests.begin();
                if(itTests!=tempTests.end()){
                    qDebug("Vamos a pedir sus parametros");
                    retrieveFromDDBB("parameters", "test", (*itTests)->idPrueba);
                }
                else{
                    emit(testFromSessionReceived(tempTests));
                    tempTests.clear();
                    itTests= tempTests.begin();
                }
            }

            // RETRIEVE NEW TEST
            else if(jsonReceived.contains("tests")){
                if(!jsonReceived["tests"].toArray().first().toObject().isEmpty()){
                    if(!jsonReceived["test"].toArray().first().toObject().contains("idVideo")){
                        try{
                            int id = jsonReceived["tests"].toArray().first().toObject()["id"].toString().toInt();
                            (*itTests)->idPrueba = id;
                            tempSession->pruebas[id] = *itTests;

                            ++itTests;
                            //Si quedan más pruebas que registrar
                            if(itTests!=tempTests.end()){
                                (*itTests)->idSesion = tempSession->idSesion;
                                sendJSON(*itTests);
                            }
                            // o si hemos terminado
                            else{
                                emit(newSession(tempSession));
                            }

                        }catch(...){  }
                    }

                }
            }

            // RETRIEVE ALL VIDEOS (Or new video)
            else if(jsonReceived.contains("allvideos")){

                if(!jsonReceived["allvideos"].toArray().first().toObject().isEmpty()){
                    if(jsonReceived["allvideos"].toArray().first().toObject().contains("description")){
                        QList<Video*> listVideos;
                        QJsonArray array = jsonReceived["allvideos"].toArray();
                        for(const QJsonValue& val: array) {

                            Video* retrievedVideo = new Video(val.toObject());
                            listVideos.append(retrievedVideo);
                        }
                        emit(videosReceived(listVideos));
                    }
                    else{
                        try{
                            int id = jsonReceived["allvideos"].toArray().first().toObject()["id"].toString().toInt();
                            tempVideo->idVideo = id;
                            emit(newVideo(tempVideo));
                            qDebug("Ha llegado un nuevo vídeo!");
                        }catch(...){  }
                    }
                }
            }

            // RETRIEVE ZONES FROM VIDEO
            else if(jsonReceived.contains("zonesfromvideo")){

                QJsonObject JsonObject = jsonReceived["zonesfromvideo"].toArray().first().toObject();
                if(JsonObject.isEmpty()) return;
                Video* retrievedVideo = new Video(JsonObject);

                QList<Zona*> listZones;
                QJsonArray arrayZones = JsonObject["zones"].toArray();
                for(const QJsonValue& val: arrayZones) {
                    Zona* retrievedZone = new Zona(val.toObject());
                    listZones.append(retrievedZone);
                }
                emit(zonesFromVideoReceived(retrievedVideo, listZones));
            }

            // RETRIEVE NEW ZONE
            else if(jsonReceived.contains("zones")){
                if(!jsonReceived["zones"].toArray().first().toObject().isEmpty()){
                    if(!jsonReceived["zones"].toArray().first().toObject().contains("idVideo")){
                        try{
                            int id = jsonReceived["zones"].toArray().first().toObject()["id"].toString().toInt();
                            tempZone->idZona= id;
                            emit(newZone(tempZone));
                            qDebug("Ha llegado una nueva zona!");
                        }catch(...){  }
                    }

                }
            }


            // RETRIEVE ALL LABELS (Or new label)
            else if(jsonReceived.contains("alllabels")){

                if(!jsonReceived["alllabels"].toArray().first().toObject().isEmpty()){
                    if(jsonReceived["alllabels"].toArray().first().toObject().contains("description")){

                        QList<Etiqueta*> listLabels;
                        QJsonArray array = jsonReceived["alllabels"].toArray();
                        for(const QJsonValue& val: array) {

                            Etiqueta* retrievedLabel = new Etiqueta(val.toObject());
                            listLabels.append(retrievedLabel);
                        }
                        emit(labelsReceived(listLabels));

                    }
                    else{
                        try{
                            int id = jsonReceived["alllabels"].toArray().first().toObject()["id"].toString().toInt();
                            tempLabel->idEtiqueta = id;
                            emit(newLabel(tempLabel));
                        }catch(...){  }
                    }
                }

            }
            // RETRIEVE PARAMETERS
            else if(jsonReceived.contains("parametersfromtest")){
                if(!jsonReceived["parametersfromtest"].toArray().first().toObject().isEmpty()){

                    //Rellenamos con los parámetros
                    QJsonArray array = jsonReceived["parametersfromtest"].toArray();
                    for(const QJsonValue& val: array){

                        qDebug(val.toObject()["idParameter"].toString().toUtf8());
                       (*itTests)->fillWithParams(val.toObject());
                        qDebug(QString::number((*itTests)->num_Fijaciones).toUtf8());
                    }
                }
                ++itTests;
                if(itTests!=tempTests.end()){
                    retrieveFromDDBB("parameters", "test", (*itTests)->idPrueba);
                }
                else{
                    itTests= tempTests.begin();
                    if(itTests!=tempTests.end()){
                        retrieveFromDDBB("fixations", "test", (*itTests)->idPrueba);
                    }
                }

            }
            // RETRIEVE FIXATIONS
            else if(jsonReceived.contains("fixationsfromtest")){
                if(!jsonReceived["fixationsfromtest"].toArray().first().toObject().isEmpty()){

                    QJsonArray array = jsonReceived["fixationsfromtest"].toArray();
                    for(const QJsonValue& val:array){
                        Fixation* newFixation = new Fixation(val.toObject());
                        (*itTests)->fixationsList.append(*newFixation);
                    }

                }
                ++itTests;
                if(itTests!=tempTests.end()){
                    retrieveFromDDBB("fixations", "test", (*itTests)->idPrueba);
                }
                else
                    emit(testFromSessionReceived(tempTests));
                    tempTests.clear();
                    itTests= tempTests.begin();
            }

            // HEMOS RECIBIDO CUALQUIER OTRA COSA
            else{
                qDebug() << "No tengo ni idea de que JSON es este..." << endl;
            }
        }

        // ERRORES
        else{
            qDebug() << "El archivo recibido no se podía convertir al formato JSON." << endl;
        }
    }
    else{
        qDebug(reply->readAll());
    }

    reply->deleteLater();
}
